<?php

namespace app\common\rbac;

use app\api\modules\v1\models\ApiUser;
use app\api\modules\v1\models\Project;
use app\api\modules\v1\models\UserRole;
use yii\rbac\Rule;


class CheckUserRole extends Rule
{
    public $name = 'checkUserRole';

    public function execute($apiUserId, $item, $params = [])
    {
        $defaults = [
            'role' => '',
            'params' => [
                'except' => []
            ]
        ];
        $opts = array_merge($defaults, $params);

        //Get API user from api_users tables by received id.
        $apiUser = ApiUser::findOne($apiUserId);
        if (!$apiUser) {
            return false;
        }
        if (!$apiUser->user) {
            return false;
        }

        //Get all roles available for the user through the api user (from the table users)
        $userRoles = $apiUser->user->roles;

        //User roles passed as a string
        if (!is_array($opts['role'])) {
            //If passed roles not "any", check it againsta user available roles
            if ($opts['role'] != 'any' && !in_array(strtolower($opts['role']), $userRoles)) {
                return false;
            }

            //Passed "any" for user role, meaning any role can perform this action. Need to check against exceptions (if any)
            if ($opts['role'] === 'any') {
                //Define and/or convert passed exceptions to the array
                if (!isset($opts['params']['except'])) {
                    $opts['params']['except'] = [];
                }
                else {
                    if (!is_array($opts['params']['except'])) {
                        $opts['params']['except'] = explode(',', $opts['params']['except']);
                    }
                }

                $opts['params']['except'] = array_map('strtolower', $opts['params']['except']);
                $allRoles = UserRole::allRoles();
                $allowedRoles = array_diff($allRoles, $opts['params']['except']);
                $intersectedRoles = array_intersect($userRoles, $allowedRoles);
                if (empty($intersectedRoles)) {
                    return false;
                }
            }
        }

        //User roles passed as a array of roles
        if (is_array($opts['role']) && !empty($opts['role'])) {
            $status = false;
            foreach ($opts['role'] as $key => $role) {
                if (in_array(strtolower($role), $userRoles)) {
                    $status = true;
                }
            }

            if (!$status) {
                return $status;
            }
        }

        //If project id is defined, check its account_id against received account
        if (isset($opts['params']['project_id'])) {
            $project = Project::findOne($opts['params']['project_id']);
            if ($project && $apiUser->user->lastAccount && $project->account_id != $apiUser->user->lastAccount->id) {
                return false;
            }
        }

        //If account id is defined, check whether current user has access to this account
        if (isset($opts['params']['account_id']) && !in_array($opts['params']['account_id'], $apiUser->user->relatedAccountIds)) {
            return false;
        }

        return true;
    }
}