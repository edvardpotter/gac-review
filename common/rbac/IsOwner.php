<?php

namespace app\common\rbac;

use app\api\modules\v1\models\ApiUser;
use yii\rbac\Rule;

class IsOwner extends Rule
{
    public $name = 'IsOwner';

    public function execute($user, $item, $params = [])
    {
        $defaults = ['user_id' => 0];
        $opts = array_merge($defaults, $params);
        $apiUser = ApiUser::findOne($user);
        if (!$apiUser) {
            return false;
        }

        return $opts['user_id'] === $apiUser->user_id;
    }
}