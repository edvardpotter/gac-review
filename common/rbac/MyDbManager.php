<?php

namespace app\common\rbac;

use app\components\Utils;
use yii\rbac\DbManager;

class MyDbManager extends DbManager
{
    public function init()
    {
        parent::init();
        //Override standard db initialization with custom
        $this->db = Utils::getDb();
    }
}