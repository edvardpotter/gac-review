<?php
$config = 'api_prod.php';

//setup sandbox enviroinment
if (@file_exists(dirname(__FILE__) . '/../sandbox')) {
    $config = 'api_prod_sandbox.php';
}

//setup sandbox-dev enviroinment
if (@file_exists(dirname(__FILE__) . '/../sandbox-dev')) {
    $config = 'api_prod_sandbox-dev.php';
}

//comment out the following two lines when deployed to production
if (@file_exists(dirname(__FILE__) . '/../stage')) {
    $config = 'api_stage.php';
}

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');


require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

clearstatcache();

$config = require(__DIR__ . '/../config/' . $config);

(new yii\web\Application($config))->run();
