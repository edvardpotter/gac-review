<?php

namespace app\api\modules\v1\components;

use app\api\modules\v1\models\Client;
use app\api\modules\v1\models\Copywriter;
use app\api\modules\v1\models\User;
use Segment;
use Yii;
use yii\base\Component;


/**
 * Class Segmentio
 * @package app\api\modules\v1\components
 */
class Segmentio extends Component
{

    /**
     * @var array
     */
    public $params = [];
    /**
     * @var string
     */
    public $userId = '';
    /**
     * @var string
     */
    public $userType = '';

    /**
     * @return bool|void
     */
    public function init()
    {
        parent::init();

        if (isset(Yii::$app->params['segmentio']) && Yii::$app->params['segmentio']) {
            Segment::init($this->params['write_key'], ["consumer" => "fork_curl", "debug" => false]);
        }

        if (isset(Yii::$app->params['user']['id'])) {
            $this->userId = Yii::$app->params['user']['id'];
        }
        if (isset(Yii::$app->params['user']['type'])) {
            $this->userType = Yii::$app->params['user']['type'];
        }

        if (!isset(Yii::$app->params['segmentio']) || (isset(Yii::$app->params['segmentio']) && !Yii::$app->params['segmentio'])) {
            return false;
        }

    }

    /**
     * @param $options
     * @return bool
     */
    public function track($options)
    {
        $SegmentioDefaults = [
            'anonymousId' => '',
            'userId' => '',
            'alias' => false,
            'do_track' => true
        ];

        if (isset($_SESSION)) {
            $SegmentioJson = self::segmentioSession();
        } else {
            $SegmentioJson = $SegmentioDefaults;
        }

        if (!$SegmentioJson['do_track']) {
            return false;
        }

        $EventIn = array(
            'BrowsePage',
            'BrowseProjectDetails',
            'Login',
            'Logout',
            'RestorePassword',
            'Registration',
            'SupportBttnClick',
            'SubmitPitch',
            'RejectPitch',
            'AwardPitch',
            'SaveRevision',
            'SubmitRevision',
            'RequestRevision',
            'SaveAccount',
            'NewOrder',
            'ApplySpecialityPopup',
            'ApplySpecialitySubmit',
            'AddProduct',
            'CompleteOrder',
            'ApproveProject',
            'UpdateProject',
            'DuplicateProject',
            'InviteWriter',
            'UpdatePassword',
            'DownloadProject',
            'ExtendDue',
            'NewProspect',
            'CompletedSurvey',
            'ClickShowChanges',
        );

        $EventOut = array(
            'Viewed',
            'Viewed Project Details',
            'Logged In',
            'Logged Out',
            'Restored Password',
            'Created Account',
            'Clicked Intercom',
            'Submitted Pitch',
            'Rejected Pitch',
            'Awarded Pitch',
            'Saved Revision',
            'Submitted Revision',
            'Requested Revision',
            'Updated Account Details',
            'Placed Order',
            'Started Speciality Application',
            'Completed Speciality Application',
            'Added Product',
            'Completed Order',
            'Approved Project',
            'Updated Project',
            'Duplicated Project',
            'Invited Writer',
            'Updated Password',
            'Downloaded Project',
            'Extended Review',
            'Became Prospect',
            'Completed survey',
            'Clicked Show Changes',
        );


        $RestrictedStatus = false;
        $Restricted = [
            'userAgent' => ['Rackspace Monitoring'],
        ];

        foreach ($Restricted as $Key => $Value) {
            if ($Key == 'userAgent') {
                foreach ($Value as $Key => $SubValue) {
                    if (isset($_SERVER['HTTP_USER_AGENT']) && strpos($_SERVER['HTTP_USER_AGENT'], $SubValue) !== false) {
                        $RestrictedStatus = true;
                    }
                }
            }
        }

        if ($RestrictedStatus) {
            return false;
        }

        $defaults = [
            'event' => '',
            'userId' => '',
            'userType' => ''
        ];

        $user = [
            'userId' => '',
            'userType' => '',
            'anonymousId' => '',
            'firstName' => '',
            'lastName' => '',
            'email' => '',
            'username' => ''
        ];

        $Traits = [];
        $OptsProperties = [];

        $opts = array_merge($defaults, $options);
        if (isset($opts['properties']) && !empty($opts['properties'])) {
            $OptsProperties = $opts['properties'];
        }
        if (isset($opts['traits']) && !empty($opts['traits'])) {
            $Traits = $opts['traits'];
        }


        $TrackData = [
            'event' => '',
            'properties' => [
                'referrer' => (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : '',
                'Session ID' => Yii::$app->session->getId()
            ],
            'context' => [
                'ip' => Yii::$app->request->remoteIP,
                'userAgent' => (isset($_SERVER['HTTP_USER_AGENT'])) ? $_SERVER['HTTP_USER_AGENT'] : '',
                'last_request_at' => time()
            ]
        ];

        $user['userId'] = Yii::$app->params['user']['id'];
        $user['userType'] = Yii::$app->params['user']['type'];

        if ($opts['userId'] != '') {
            $user['userId'] = $opts['userId'];
        }
        if ($opts['userType'] != '') {
            $user['userType'] = $opts['userType'];
        }

        if ($user['userId'] == '') {

            //Setting up userId for the prospect
            if (isset(Yii::$app->session) && isset(Yii::$app->session['PROSPECT_ID'])) {
                $user['userId'] = Yii::$app->session['PROSPECT_ID'];
                $Tmp = new Prospect($user['userId']);
                $user['userType'] = 'Prospect';
                $user['email'] = $Tmp->email;

                $TrackData['userId'] = $user['userType'] . '_' . $user['userId'];
                if (Yii::$app->params['segmentioUniqueID']) {
                    $TrackData['userId'] = $Tmp->uniqueID;
                }
            }

            //Setting up userId for the Lead
            if ($user['userId'] == '' && isset($Traits['lead_id']) && $Traits['lead_id'] != '') {
                $leads = new Leads();
                $tmp = $leads->getLead(array('filter' => 'l.unique_id LIKE(\'' . $Traits['lead_id'] . '\')'));
                if (!empty($tmp) && isset($tmp[0])) {
                    $lead = $tmp[0];
                    $user['userId'] = $lead['chat_id'];
                    $user['userType'] = 'Lead';
                    $user['email'] = $lead['email'];
                    $TrackData['userId'] = $user['userId'];
                }
            }
        }


        //Setting up anonymous_id
        if (Yii::$app->params['is_console']) {
            $defaultAnonymousId = 'console_at_' . time();
        } else {
            $defaultAnonymousId = Yii::$app->session->getId();
        }

        if (!isset($opts['anonymousId']) || (isset($opts['anonymousId']) && $opts['anonymousId'] == '')) {
            $anonymousId = $defaultAnonymousId;
        } else {
            $anonymousId = $opts['anonymousId'];
        }


        if (!Yii::$app->params['is_console'] && isset(Yii::$app->request->cookies['_sio'])) {
            $Tmp = explode('----', Yii::$app->request->cookies['_sio']);
            $anonymousId = $Tmp[0];
        }

        $user['anonymousId'] = $TrackData['anonymousId'] = $anonymousId;


        if ($user['userType'] == 'user') {
            $model = User::findOne($user['userId']);

            $user['userType'] = 'User';
            $user['firstName'] = $model->first_name;
            $user['lastName'] = $model->last_name;
            $user['email'] = $user['username'] = $model->email;

            $TrackData['userId'] = $user['userType'] . '_' . $user['userId'];
            if (Yii::$app->params['segmentioUniqueID']) {
                $TrackData['userId'] = $model->unique_id;
            }
        }

        if ($user['userType'] == 'writer') {
            $copywriter = Copywriter::findOne($user['userId']);

            $user['userType'] = 'Writer';
            $user['firstName'] = $copywriter->first_name;
            $user['lastName'] = $copywriter->last_name;
            $user['email'] = $user['username'] = $copywriter->email;

            $TrackData['userId'] = $user['userType'] . '_' . $user['userId'];
            if (Yii::$app->params['segmentioUniqueID']) {
                $TrackData['userId'] = $copywriter->unique_id;
            }
        }

        $RequestUrl = (isset($_SERVER['REQUEST_URI'])) ? $_SERVER['REQUEST_URI'] : '';



        if ($opts['event'] != '') {
            $TrackData['event'] = str_replace($EventIn, $EventOut, $opts['event']);

            if ($opts['event'] == 'BrowsePage') {
                $TrackData['event'] .= ' [' . $RequestUrl . ']';
                $TrackData['event'] = mb_strcut($TrackData['event'], 0, 99) . ']';
            }


            $TrackData['properties'] = array_merge(
                $TrackData['properties'],
                $OptsProperties
            );
        }


        if ($TrackData['event'] != '') {
            if (!isset($TrackData['userId'])) {
                $TrackData['userId'] = '';
            }

            Segment::track($TrackData);
            $IdentifyEvents = ['Login', 'Registration', 'SaveAccount', 'CompleteOrder', 'NewProspect', 'CompletedSurvey', 'Became Lead'];

            if (in_array($opts['event'], $IdentifyEvents)) {
                $this->identify(['traits' => $Traits, 'userId' => $opts['userId'], 'userType' => $opts['userType']]);
            }

        }

        //Save to session if not running from the console (cron)
        if (!Yii::$app->params['is_console']) {
            $SegmentioJson['anonymousId'] = (isset($TrackData['anonymousId'])) ? $TrackData['anonymousId'] : '';
            $SegmentioJson['userId'] = (isset($TrackData['userId'])) ? $TrackData['userId'] : '';
            self::segmentioSession($SegmentioJson);
        }
    }

    /**
     * @param array $options
     * @return mixed
     */
    public static function segmentioSession($options = [])
    {
        $defaults = [
            'anonymousId' => '',
            'userId' => '',
            'alias' => false,
            'do_track' => true
        ];

        $opts = array_merge($defaults, $options);

        if (!isset(Yii::$app->session['SEGMENTIO'])) {
            Yii::$app->session['SEGMENTIO'] = json_encode($opts);
        } else {
            $session_data = json_decode(Yii::$app->session['SEGMENTIO'], true);
            $stored_data = array_merge($defaults, $session_data);
            Yii::$app->session['SEGMENTIO'] = json_encode(array_merge($stored_data, $options));
        }

        return json_decode(Yii::$app->session['SEGMENTIO'], true);
    }

    /**
     * @param array $options
     * @return bool
     */
    public function identify($options = [])
    {
        $SegmentioDefaults = [
            'anonymousId' => '',
            'userId' => '',
            'alias' => false,
            'do_track' => true
        ];

        $anonymousId = time(); //Default anonymousId.

        if (isset($_SESSION)) {
            $SegmentioJson = self::segmentioSession();
        } else {
            if (isset($options['segmentiojson'])) {
                $SegmentioJson = $options['segmentiojson'];
            }
            else {
                $SegmentioJson = $SegmentioDefaults;
            }
        }

        if (!$SegmentioJson['do_track']) {
            return false;
        }

        if (!Yii::$app->params['is_console']) {
            $anonymousId = Yii::$app->session->getId();
        }

        if (isset(Yii::$app->request->cookies['_sio'])) {
            $Tmp = explode('----', Yii::$app->request->cookies['_sio']);
            $anonymousId = $Tmp[0];
        }

        $TrackData = [
            'traits' => [],
            'context' => [
                'ip' => Yii::$app->request->remoteIP,
                'userAgent' => (isset($_SERVER['HTTP_USER_AGENT'])) ? $_SERVER['HTTP_USER_AGENT'] : '',
                'last_request_at' => time(),
            ],
        ];

        $Traits = [
            'User ID' => '',
            'User Type' => '',
            'First Name' => '',
            'Last Name' => '',
            'email' => '',
            'Account Country' => '',
            'Paypal Account' => '',
            'Notification Frequency' => '',
            'Historical Revenue' => '',
            'NPS' => '',
            'Background' => '',
            'Path' => '',
            'Session ID' => Yii::$app->session->getId(),
        ];

        if (isset($options['traits']) && !empty($options['traits'])) {
            $Traits = array_merge($Traits, $options['traits']);
        }

        if ((!isset($anonymousId) || (isset($anonymousId) && $anonymousId == '')) && Yii::$app->params['is_console'] && isset($Traits['anonymousId'])) {
            $anonymousId = $Traits['anonymousId'];
        }

        //Override userId with the received through the options array
        if (!empty($options['userId'])) {
            $this->userId = $options['userId'];
        }
        if (!empty($options['userType'])) {
            $this->userType = $options['userType'];
        }

        if ($this->userId !== '' && $this->userType !== '') {
            $Traits['User ID'] = $this->userId;
        } else {
            $TrackData['anonymousId'] = $anonymousId;
        }

        if ($this->userId !== '' && $this->userType === 'user') {
            $user = User::findOne($Traits['User ID']);
            $Traits['User Type'] = 'User';
            $Traits['First Name'] = $user->first_name;
            $Traits['Last Name'] = $user->last_name;
            $Traits['email'] = $user->email;

            $Traits['Account Country'] = $user->relatedCompany && $user->relatedCompany->country ? $user->relatedCompany->country->name : '';
            $Traits['Historical Revenue'] = $user->relatedCompany ? $user->relatedCompany->projectsTotalAmount : '';
            $Traits['Background'] = $user->relatedCompany && $user->relatedCompany->background ? $user->relatedCompany->background->name : '';


            $TrackData['userId'] = $Traits['User Type'] . '_' . $Traits['User ID'];
            if (Yii::$app->params['segmentioUniqueID']) {
                $TrackData['userId'] = $user->unique_id;
            }
        }

        if ($this->userId !== '' && $this->userType === 'writer') {
            $copywriter = Copywriter::findOne($Traits['User ID']);
            $Traits['User Type'] = 'Writer';
            $Traits['First Name'] = $copywriter->first_name;
            $Traits['Last Name'] = $copywriter->last_name;
            $Traits['email'] = $copywriter->email;

            $Traits['Account Country'] = $copywriter->relatedCountry ? $copywriter->relatedCountry->name : '';
            $Traits['Paypal Account'] = $copywriter->paypal_account;
            $Traits['Notification Frequency'] = $copywriter->dailyDigestText;
            $Traits['Historical Revenue'] = round($copywriter->projectsTotalAmount());

            $TrackData['userId'] = $Traits['User Type'] . '_' . $Traits['User ID'];
            if (Yii::$app->params['segmentioUniqueID']) {
                $TrackData['userId'] = $copywriter->unique_id;
            }
        }


        if (!Yii::$app->params['is_console']) {
            $routeArr = Yii::$app->urlManager->parseRequest(Yii::$app->request);
            $route = '';
            if ($routeArr) {
                $route = $routeArr[0];
            }
        } else {
            $route = 'console';
        }

        $Traits['Path'] = $route;
        $TrackData['traits'] = array_merge($TrackData['traits'], $Traits);


        if ($Traits['User ID'] != '' && $TrackData['userId'] != '' && !$SegmentioJson['alias']) {
            Segment::alias(['previousId' => $anonymousId, 'userId' => $TrackData['userId']]);
            $SegmentioJson['alias'] = true;
        }

        if (isset(Yii::$app->session)) {
            $SegmentioJson['anonymousId'] = (isset($TrackData['anonymousId'])) ? $TrackData['anonymousId'] : '';
            $SegmentioJson['userId'] = (isset($TrackData['userId'])) ? $TrackData['userId'] : '';
            self::segmentioSession($SegmentioJson);
        }


        $TrackData['intercom'] = array(
            'user_hash' => hash_hmac('sha256', 'wEpLqX94wvwnKKpxOUHL7NGmLso5NRkvhfS7ct9F', ((isset($TrackData['userId']) && $TrackData['userId'] != '') ? $TrackData['userId'] : $TrackData['anonymousId']))
        );

        //Updating the user from admin section. Overriding all the settings, defined before in this function
        if (isset($options['mode']) && $options['mode'] == 'admin') {
            if (isset($options['userId'])) {
                $TrackData['userId'] = $options['userId'];
            }
            if (!empty($options['traits'])) {
                $TrackData['traits'] = $options['traits'];
            }
        }

        Segment::identify($TrackData);
    }
}