<?php

namespace app\api\modules\v1\components;

use app\api\modules\v1\models\Copywriter;
use app\api\modules\v1\models\Prospect;
use app\api\modules\v1\models\User;
use Segment;
use Yii;
use yii\base\Component;

/**
 * Class SegmentApi
 * @package app\api\modules\v1\components
 */
class SegmentApi extends Component
{

    /**
     * @var array
     */
    public $params = [];
    /**
     * @var string
     */
    public $userId = '';
    /**
     * @var string
     */
    public $userType = '';
    /**
     * @var array
     */
    public $identifyEvents = ['Created Account', 'Became Prospect'];


    /**
     * @return bool|void
     */
    public function init()
    {
        parent::init();

        //Check whether segment is enabled. If not - exit.
        if (!isset(Yii::$app->params['segmentApi']) || (isset(Yii::$app->params['segmentApi']) && !Yii::$app->params['segmentApi'])) {
            return false;
        }

        Segment::init($this->params['write_key'], ["consumer" => "fork_curl", "debug" => false]);

        //Set default values for userid and type based on currently logged user
        if (isset(Yii::$app->params['user']['id'])) {
            $this->userId = Yii::$app->params['user']['id'];
        }
        if (isset(Yii::$app->params['user']['type'])) {
            $this->userType = Yii::$app->params['user']['type'];
        }
    }

    /**
     * @param $options
     * @return array
     */
    public function track($options)
    {
        $defaults = [
            'event' => 'Unknown',
            'userId' => '',
            'userType' => '',
            'traits' => [],           //data to be used for Segment::identify
            'properties' => []
        ];
        $opts = array_merge($defaults, $options);

        if (Yii::$app->params['is_console']) {
            $anonymousId = 'console_at_' . time();
        } else {
            $anonymousId = Yii::$app->session->getId();
        }

        //Data to be used during Segment::track
        $trackData = [
            'event' => $opts['event'],
            'anonymousId' => $anonymousId,
            'userId' => '',
            'properties' => [
                'referrer' => isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '',
                'Session ID' => !Yii::$app->params['is_console'] ? Yii::$app->session->getId() : ''
            ],
            'context' => [
                'ip' => Yii::$app->request->getUserIP(),
                'userAgent' => isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '',
                'last_request_at' => time()
            ]
        ];

        $trackData['properties'] = array_merge($trackData['properties'], $opts['properties']);

        $traitsData = [];
        if (isset($opts['traits']) && !empty($opts['traits'])) {
            $traitsData = $opts['traits'];
        }

        //Override default user_id, solved by logged user with passed in user_id
        if ($opts['userId'] !== '') {
            $this->userId = $opts['userId'];
        }
        if ($opts['userType'] !== '') {
            $this->userType = $opts['userType'];
        }

        if ((int)$this->userId !== 0 && (string)$this->userType !== '') {
            if ($this->userType === 'user') {
                $model = User::findOne($this->userId);
                if ($model) {
                    $trackData['userId'] = $model->unique_id;
                }
            }
            if ($this->userType === 'writer') {
                $model = Copywriter::findOne($this->userId);
                if ($model) {
                    $trackData['userId'] = $model->unique_id;
                }
            }
        }


        Segment::track($trackData);
        if (in_array($trackData['event'], $this->identifyEvents)) {
            $this->identify(['traits' => $traitsData, 'userId' => $opts['userId'], 'userType' => $opts['userType']]);
        }

        return ['yii' => Yii::$app->params['user'], 'userId' => $this->userId, 'userType' => $this->userType];
    }

    /**
     * @param $options
     */
    public function identify($options)
    {
        $defaults = [];
        $opts = array_merge($defaults, $options);

        $anonymousId = time(); //Default anonymousId. Not session based is it might be possible to be envoked from console
        if (!Yii::$app->params['is_console']) {
            $anonymousId = Yii::$app->session->getId();
        }

        $trackData = [
            'traits' => [],
            'userId' => '',
            'context' => [
                'ip' => Yii::$app->request->remoteIP,
                'userAgent' => isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '',
                'last_request_at' => time()
            ],
        ];

        //Default data passed to Segment during identify
        $traits = [
            'User ID' => '',
            'User Type' => '',
            'First Name' => '',
            'Last Name' => '',
            'email' => '',
            'Account Country' => '',
            'Paypal Account' => '',
            'Notification Frequency' => '',
            'Historical Revenue' => '',
            'NPS' => '',
            'Background' => '',
            'Path' => '',
            'Session ID' => !Yii::$app->params['is_console'] ? Yii::$app->session->getId() : ''
        ];

        if (isset($opts['traits']) && !empty($opts['traits']) && is_array($opts['traits'])) {
            $traits = array_merge($traits, $options['traits']);
        }

        //Override userId with the received through the options array
        if (!empty($options['userId'])) {
            $this->userId = $options['userId'];
        }
        if (!empty($options['userType'])) {
            $this->userType = $options['userType'];
        }

        if ((int)$this->userId !== 0 && (string)$this->userType === 'user') {
            $model = User::findOne($this->userId);
            if ($model) {
                $traits['User Type'] = 'User';
                $traits['User ID'] = $model->id;
                $traits['First Name'] = $model->first_name;
                $traits['Last Name'] = $model->last_name;
                $traits['email'] = $model->email;

                $traits['Account Country'] = $model->relatedCompany && $model->relatedCompany->country ? $model->relatedCompany->country->name : '';
                $traits['Historical Revenue'] = $model->relatedCompany ? $model->relatedCompany->projectsTotalAmount : '';
                $traits['Background'] = $model->relatedCompany && $model->relatedCompany->background ? $model->relatedCompany->background->name : '';

                $trackData['userId'] = $model->unique_id;
            }
        }

        if ((int)$this->userId !== 0 && (string)$this->userType === 'writer') {
            $model = Copywriter::findOne($this->userId);
            if ($model) {
                $traits['User Type'] = 'Writer';
                $traits['First Name'] = $model->first_name;
                $traits['Last Name'] = $model->last_name;
                $traits['email'] = $model->email;

                $traits['Account Country'] = $model->relatedCountry ? $model->relatedCountry->name : '';
                $traits['Paypal Account'] = $model->paypal_account;
                $traits['Notification Frequency'] = $model->dailyDigestText;
                $traits['Historical Revenue'] = round($model->projectsTotalAmount());

                $trackData['userId'] = $model->unique_id;
            }
        }

        if ((int)$this->userId !== 0 && (string)$this->userType === 'prospect') {
            $model = Prospect::findOne($this->userId);
            if ($model) {
                $traits['User Type'] = 'Prospect';
                $traits['User ID'] = $model->id;
                $traits['email'] = $model->email;
                $trackData['userId'] = $model->unique_id;
            }
        }

        //Get current route
        if (!Yii::$app->params['is_console']) {
            $routeArr = Yii::$app->urlManager->parseRequest(Yii::$app->request);
            $route = '';
            if ($routeArr) {
                $route = $routeArr[0];
            }
        } else {
            $route = 'console';
        }

        $traits['Path'] = $route;
        $trackData['traits'] = array_merge($trackData['traits'], $traits);

        //Connect anonymous id with the real user id
        if ($trackData['userId'] != '') {
            Segment::alias(['previousId' => $anonymousId, 'userId' => $trackData['userId']]);
        }

        Segment::identify($trackData);
    }
}