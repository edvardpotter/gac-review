<?php

namespace app\api\modules\v1\components;

use app\components\Utils;
use yii;
use yii\filters\auth\AuthMethod;


/**
 * Class Authenticator
 * @package app\api\modules\v1\components
 */
class Authenticator extends AuthMethod
{

    /**
     * @var string
     */
    public $header = 'Authorization';
    /**
     * @var string
     */
    public $pattern = '/^GetACopywriter\s+(.*)$/';

    /**
     * @param yii\web\User $user
     * @param yii\web\Request $request
     * @param yii\web\Response $response
     * @return yii\web\IdentityInterface|null
     * @throws yii\web\HttpException
     * @throws yii\web\UnauthorizedHttpException
     */
    public function authenticate($user, $request, $response)
    {
        $controllerAction = Yii::$app->controller->id . '/' . Yii::$app->controller->action->id;

        $pubkey = yii::$app->request->get('pubkey');        //Retrieve public key from URL. Defined within config urlRules
        $privateKey = '';
        $authHeader = $request->getHeaders()->get($this->header);
        $error = '';

        if ($error === '' && $pubkey === null) {
            $error = 'Authorization public key not selected';
        }

        if (Yii::$app->params['authorization']['validateHttpHeader'] && !in_array($controllerAction, Yii::$app->params['authorization']['authenticateWithoutHeader'])) {
            if ($error === '' && $authHeader === null) {
                $error = 'Authorization header not selected';
            }
            if ($error === '' && !isset($this->pattern)) {
                $error = 'Authorization header pattern not found';
            }
            if ($error === '' && $authHeader !== null) {
                preg_match_all($this->pattern, $authHeader, $matches);
                if (empty($matches[1])) {
                    $error = 'Authorization token not found or malformed';
                } else {
                    $privateKey = $matches[1][0];
                }
            }
        }

        if ($error !== '') {
            throw new \yii\web\HttpException('401', Utils::errorMessage(401, $error));
        }


        $identity = $user->loginByAccessToken($privateKey, get_class($this));
        if ($identity === null) {
            $this->handleFailure($response);
        }

        return $identity;
    }
}