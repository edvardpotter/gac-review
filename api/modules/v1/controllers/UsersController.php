<?php

namespace app\api\modules\v1\controllers;


use app\api\modules\v1\models\Account;
use app\api\modules\v1\models\Copywriter;
use app\api\modules\v1\models\PasswordRestore;
use app\api\modules\v1\models\Prospect;
use app\api\modules\v1\models\User;
use app\components\Utils;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class UsersController
 * @package app\api\modules\v1\controllers
 */
class UsersController extends GlobalController
{

    /**
     * @var string
     */
    public $modelClass = 'app\api\modules\v1\models\User';

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access'] = [
            'class' => \yii\filters\AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@']
                ],
                [
                    'actions' => ['add-prospect', 'set-password', 'restore-password', 'email-exists'],
                    'allow' => true,
                    'roles' => ['?']
                ],

            ]
        ];

        //Removing default verbFilter
        unset($behaviors['verbFilter']);
        return $behaviors;
    }

    /**
     * @return ActiveDataProvider
     * @throws \yii\web\HttpException
     */
    public function actionIndex()
    {

        if (!Yii::$app->user->can('actAs', ['role' => ['administrator', 'manager']])) {
            throw new \yii\web\HttpException('403', Utils::errorMessage(403, 'role based permission'));
        }

        $user = User::findOne(Yii::$app->params['user']['id']);
        if (!$user) {
            throw new \yii\web\HttpException('404', Utils::errorMessage(404));
        }

        $sql = "SELECT u.* FROM users u WHERE u.id IN (SELECT u1.id FROM users u1 WHERE IFNULL(u1.active,0)=1 AND u1.company_id=" . (int)$user->company_id . ") AND u.user_role_id<=5";
        $query = User::findBySql($sql);
        $dataProvider = new ActiveDataProvider(['query' => $query, 'pagination' => false]);
        return $dataProvider;
    }


    /**
     * @return User
     * @throws \yii\web\HttpException
     */
    public function actionCreate()
    {
        $params = Yii::$app->request->bodyParams;
        if (!Yii::$app->user->can('actAs', ['role' => ['administrator', 'manager']])) {
            throw new \yii\web\HttpException('403', Utils::errorMessage(403, 'role based permission'));
        }

        $params['currentUserId'] = Yii::$app->params['user']['id'];
        $model = new User();
        $model->scenario = User::SCENARIO_CREATE;
        $model->load($params, '');
        if (!$model->save()) {
            throw new \yii\web\HttpException('400', Utils::errorMessage(400, implode("\n", $model->getFirstErrors())));
        }

        Utils::log('api_client', 'User created (id: ' . $model->id . ')');
        return $model;
    }


    /**
     * @return mixed
     * @throws \yii\web\HttpException
     */
    public function actionUpdate()
    {
        if (!Yii::$app->user->can('actAs', ['role' => ['administrator', 'manager']])) {
            throw new \yii\web\HttpException('403', Utils::errorMessage(403, 'role based permission'));
        }

        $params = Yii::$app->request->bodyParams;
        $params['currentUserId'] = Yii::$app->params['user']['id'];
        $model = $this->findModel(Yii::$app->request->get('user_uid'));
        $model->scenario = User::SCENARIO_UPDATE;
        $model->load($params, '');
        if (!$model->save()) {
            throw new \yii\web\HttpException('400', Utils::errorMessage(400, implode("\n", $model->getFirstErrors())));
        }

        return $model;
    }

    /**
     * @param $id
     * @return mixed
     * @throws \yii\web\HttpException
     */
    protected function findModel($id)
    {
        if (($model = $this->modelClass::findOne(['unique_id' => $id])) !== null) {
            return $model;
        }
        else {
            throw new \yii\web\HttpException('404', Utils::errorMessage(404, $id));
        }
    }

    /**
     * @return User|null
     */
    public function actionDetails()
    {
        return User::findOne(Yii::$app->params['user']['id']);
    }

    /**
     * @return User|null
     * @throws \yii\web\HttpException
     */
    public function actionKeys()
    {
        $ip = ['87.246.174.130'];
        if (in_array(Yii::$app->params['user']['ip'], $ip)) {
            return User::findOne(Yii::$app->request->get('user_uid'));
        }
        throw new \yii\web\HttpException('404', 'Page not found');
    }

    /**
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionDelete()
    {
        if (!Yii::$app->user->can('actAs', ['role' => ['administrator']])) {
            throw new \yii\web\HttpException('403', Utils::errorMessage(403, 'role based permission'));
        }
        $model = $this->findModel(Yii::$app->request->get('unique_id'));

        $params['currentUserId'] = Yii::$app->params['user']['id'];
        $model->load($params, '');
        if (!$model->actionDelete()) {
            throw new \yii\web\HttpException('400', Utils::errorMessage(400, $model->customError));
        }
        return 'Success';
    }

    /**
     * @return mixed
     * @throws \yii\web\HttpException
     */
    public function actionAddAccount()
    {
        if (!Yii::$app->user->can('actAs', ['role' => ['administrator', 'manager']])) {
            throw new \yii\web\HttpException('403', Utils::errorMessage(403, 'role based permission'));
        }

        $user = User::findOne(Yii::$app->params['user']['id']);
        $model = $this->findModel(Yii::$app->request->get('user_uid'));
        $account = $this->findAccount(Yii::$app->request->get('account_unique_id'));

        if ($model->company_id != $account->company_id) {
            throw new \yii\web\HttpException('403', Utils::errorMessage(403, 'account not found within your company'));
        }
        if ($user->company_id != $model->company_id) {
            throw new \yii\web\HttpException('403', Utils::errorMessage(403, 'user not found within your company'));
        }
        if (!$model->assignAccounts([0 => $account->id])) {
            throw new \yii\web\HttpException('400', Utils::errorMessage(400, ($model->customError !== '' ? $model->customError : 'unable to add account')));
        }

        $model->refresh();
        return $model;
    }

    /**
     * @param $id
     * @return mixed
     * @throws \yii\web\HttpException
     */
    protected function findAccount($id)
    {
        if (($model = Account::findOne(['unique_id' => $id])) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException('404', Utils::errorMessage(404, $id));
        }
    }

    /**
     * @return mixed
     * @throws \yii\web\HttpException
     */
    public function actionDeleteAccount()
    {
        if (!Yii::$app->user->can('actAs', ['role' => ['administrator', 'manager']])) {
            throw new \yii\web\HttpException('403', Utils::errorMessage(403, 'role based permission'));
        }

        $user = User::findOne(Yii::$app->params['user']['id']);
        $model = $this->findModel(Yii::$app->request->get('unique_id'));
        $account = $this->findAccount(Yii::$app->request->get('account_unique_id'));

        if ($model->company_id != $account->company_id) {
            throw new \yii\web\HttpException('403', Utils::errorMessage(403, 'account not found within your company'));
        }
        if ($user->company_id != $model->company_id) {
            throw new \yii\web\HttpException('403', Utils::errorMessage(403, 'you are not allowed to perform actions on this user'));
        }
        if (!$model->removeAccounts([0 => $account->id])) {
            throw new \yii\web\HttpException('400', Utils::errorMessage(400, 'unable to remove account'));
        }

        $model->refresh();
        return $model;
    }

    /**
     * @return string
     */
    public function actionEmailExists()
    {
        $params = array_merge(['email' => ''], Yii::$app->request->queryParams);

        $user = User::checkExists($params['email']);
        $writer = Copywriter::checkExists($params['email']);

        if ($user === false && $writer === false) {
            return 'no';
        }
        return 'yes';
    }

    /**
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionRestorePassword()
    {
        $params = Yii::$app->request->queryParams;

        $model = new PasswordRestore();
        $model->scenario = PasswordRestore::SCENARIO_RESTORE;
        $model->load($params, '');
        if (!$model->validate()) {
            throw new \yii\web\HttpException('400', Utils::errorMessage(400, implode("\n", $model->getFirstErrors())));
        }
        if (!$model->restore()) {
            throw new \yii\web\HttpException('400', Utils::errorMessage(400, implode("\n", $model->getFirstErrors())));
        }

        return 'Success';
    }

    /**
     * @return array
     * @throws \yii\web\HttpException
     */
    public function actionSetPassword()
    {
        $params = Yii::$app->request->bodyParams;

        $model = new PasswordRestore();
        $model->scenario = PasswordRestore::SCENARIO_SETNEW;
        $model->load($params, '');
        if (!$model->validate()) {
            throw new \yii\web\HttpException('400', Utils::errorMessage(400, implode("\n", $model->getFirstErrors())));
        }
        if (!$model->setNew()) {
            throw new \yii\web\HttpException('400', Utils::errorMessage(400, implode("\n", $model->getFirstErrors())));
        }

        return ['status' => 'Success', 'email' => ($model->user ? $model->user->email : null)];
    }

    /**
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionAddProspect()
    {
        $params = Yii::$app->request->bodyParams;

        $model = new Prospect();
        $model->scenario = Prospect::SCENARIO_CREATE;
        $model->load($params, '');
        if (!$model->save()) {
            throw new \yii\web\HttpException('400', Utils::errorMessage(400, implode("\n", $model->getFirstErrors())));
        }

        return 'Success';
    }

    /**
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index']);
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);
        return $actions;
    }
}