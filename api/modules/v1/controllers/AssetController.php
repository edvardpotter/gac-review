<?php

namespace app\api\modules\v1\controllers;

use app\api\modules\v1\models\Asset;
use app\api\modules\v1\models\CompanyBudget;
use app\api\modules\v1\models\ConfigBundle;
use app\api\modules\v1\models\ConfigRate;
use app\api\modules\v1\models\ConfigTurnaround;
use app\api\modules\v1\models\ConfigWordCount;
use app\api\modules\v1\models\ContentType;
use app\api\modules\v1\models\CopyStyle;
use app\api\modules\v1\models\CopyVoice;
use app\api\modules\v1\models\Country;
use app\api\modules\v1\models\Currency;
use app\api\modules\v1\models\Format;
use app\api\modules\v1\models\Formatting;
use app\api\modules\v1\models\Industry;
use app\api\modules\v1\models\Localization;
use app\api\modules\v1\models\ProjectStatus;
use app\api\modules\v1\models\TargetAudience;
use app\api\modules\v1\models\UserRole;


/**
 * Class AssetController
 * @package app\api\modules\v1\controllers
 */
class AssetController extends GlobalController
{
    /**
     * @var string
     */
    public $modelClass = 'app\api\modules\v1\models\Asset';


    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        //Inherit authentication from GlobalController
        //These endpoints does not require public key
        $behaviors['authenticator']['except'] = array_merge($behaviors['authenticator']['except'], [
            'industries', 'formats', 'project-statuses', 'copystyles', 'copyvoices', 'contenttypes', 'targetaudiences', 'localizations', 'formattings'
        ]);

        return $behaviors;
    }

    /**
     * @return mixed
     */
    public function actionSuggestedWordCount()
    {
        return ConfigWordCount::find()->all();
    }

    /**
     * @return mixed
     */
    public function actionContentBudgets()
    {
        return CompanyBudget::find()->all();
    }

    /**
     * @return mixed
     */
    public function actionCountries()
    {
        return Country::find()->all();
    }

    /**
     * @return mixed
     */
    public function actionCurrencies()
    {
        return Currency::find()->all();
    }

    /**
     * @return mixed
     */
    public function actionTurnaround()
    {
        return ConfigTurnaround::find()->all();
    }

    /**
     * @return mixed
     */
    public function actionBundles()
    {
        return ConfigBundle::find()->all();
    }

    /**
     * @return mixed
     */
    public function actionRates()
    {
        return ConfigRate::find()->all();
    }

    /**
     * @return \app\api\modules\v1\models\ApiToken[]|UserRole[]|array|\yii\db\ActiveRecord[]
     */
    public function actionUserRoles()
    {
        return UserRole::find()->all();
    }

    /**
     * @return mixed
     */
    public function actionIndustries()
    {
        return Industry::find()->all();
    }

    /**
     * @return mixed
     */
    public function actionFormats()
    {
        return Format::find()->all();
    }

    /**
     * @return mixed
     */
    public function actionProjectStatuses()
    {
        return ProjectStatus::find()->all();
    }

    /**
     * @return mixed
     */
    public function actionCopystyles()
    {
        return CopyStyle::find()->all();
    }

    /**
     * @return mixed
     */
    public function actionCopyvoices()
    {
        return CopyVoice::find()->all();
    }

    /**
     * @return mixed
     */
    public function actionContenttypes()
    {
        return ContentType::find()->all();
    }

    /**
     * @return mixed
     */
    public function actionTargetaudiences()
    {
        return TargetAudience::find()->all();
    }

    /**
     * @return mixed
     */
    public function actionLocalizations()
    {
        return Localization::find()->all();
    }

    /**
     * @return mixed
     */
    public function actionFormattings()
    {
        return Formatting::find()->all();
    }

}