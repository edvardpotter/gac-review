<?php

namespace app\api\modules\v1\controllers;

use app\api\modules\v1\components\Authenticator;
use app\api\modules\v1\models\ApiEndpointException;
use app\components\Utils;
use Yii;
use yii\rest\ActiveController;

/**
 * Class GlobalController
 * @package app\api\modules\v1\controllers
 */
class GlobalController extends ActiveController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        unset($behaviors['authenticator']);

        //Configure CORS before Authenticator. Otherwise Authenticator unable to authenticate header Authorization
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                // restrict access to domains:
                'Origin' => ["*"],
                'Access-Control-Allow-Origin' => ["*"],
                'Access-Control-Request-Headers' => ["Authorization", "Accept", "Content-Type"],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Allow-Credentials' => false,
                'Access-Control-Max-Age' => 180, //Cache (in seconds)
            ],
        ];

        //Setup Authenticator.
        //check-no-auth - check API availability without passing public/private keys
        $behaviors['authenticator'] = [
            'class' => Authenticator::className(),
            'except' => [
                'check-no-auth', 'login', 'turnaround', 'bundles', 'suggested-word-count', 'content-budgets', 'countries', 'currencies', 'email-exists', 'rates', 'user-roles',
                'restore-password', 'set-password', 'add-prospect', 'support-phone',
            ],
        ];
        return $behaviors;
    }


    /**
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function init()
    {
        parent::init();
        //Setting up current datetime. Everything will work according to DB time
        $db = Utils::getDb();
        Yii::$app->params['now'] = $db->createCommand('SELECT NOW()')->queryScalar();
        preg_match("/dbname=([^;]*)/", $db->dsn, $matches);
        if (isset($matches[1])) {
            Yii::$app->params['dbname'] = $matches[1];
        }

        $endpoints = ApiEndpointException::find()->asArray()->all();
        if (!empty($endpoints)) {
            foreach ($endpoints AS $key => $value) {
                Yii::$app->params['authorization']['authenticateWithoutAccount'][] = $value['method'] . ',' . $value['endpoint'];
            }
        }


        if (empty(Yii::$app->session->get('anonymousId'))) {
            Yii::$app->session->set('anonymousId', Yii::$app->session->getId());
        }
        if (!empty(Yii::$app->session->get('anonymousId'))) {
            Yii::$app->params['user']['anonymousId'] = Yii::$app->session->get('anonymousId');
        }


        if (empty(Yii::$app->session->get('SIGNUP_REFERRER'))) {
            if (!empty($_SERVER["HTTP_REFERER"])) {
                Yii::$app->session->set('SIGNUP_REFERRER', $_SERVER["HTTP_REFERER"]);
            } else {
                Yii::$app->session->set('SIGNUP_REFERRER', '');
            }
        }
    }

    /**
     * @param $action
     * @param $result
     * @return mixed
     */
    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);

        Yii::$app->params['executionTime']['endedOn'] = microtime(true);
        Yii::$app->params['executionTime']['total'] = Yii::$app->params['executionTime']['endedOn'] - Yii::$app->params['executionTime']['startedAt'];
        return $result;
    }
}