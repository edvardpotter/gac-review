<?php

namespace app\api\modules\v1\models;

use app\api\modules\v1\models\common\MyActiveRecord;


/**
 * Class UserRole
 * @package app\api\modules\v1\models
 */
class UserRole extends MyActiveRecord
{

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'user_roles';
    }

    /**
     * @param $roleId
     * @param $roles
     */
    public static function getRoles($roleId, &$roles)
    {
        $role = static::find()->where(['id' => $roleId])->one();
        if ($role) {
            $roles[] = strtolower($role->name);
            if ((int)$role->inherit_id !== 0) {
                self::getRoles((int)$role->inherit_id, $roles);
            }
        }
    }

    /**
     * @return array
     */
    public static function allRoles()
    {
        $out = [];
        $roles = static::find()->select('name')->asArray()->all();
        foreach ($roles as $key => $role) {
            $out[] = strtolower($role['name']);
        }
        return $out;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['inherit_id'], 'integer'],
            [['name'], 'string', 'max' => 20],
            [['description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'inherit_id' => 'Inherit ID',
        ];
    }

    /**
     * @return array|false
     */
    public function fields()
    {
        return [
            'user_role' => function () {
                return strtolower($this->name);
            },
            'description'
        ];
    }

}
