<?php

namespace app\api\modules\v1\models;

use app\api\modules\v1\models\Account;
use app\api\modules\v1\models\common\MyActiveRecord;
use app\components\Utils;
use Yii;
use yii\db\Expression;
use yii\web\IdentityInterface;

/**
 * Class ApiUser
 * @package app\api\modules\v1\models
 */
class ApiUser extends MyActiveRecord implements IdentityInterface
{


    /**
     * @return string
     */
    public static function tableName()
    {
        return 'api_users';
    }

    /**
     * @param int|string $id
     * @return ApiUser|IdentityInterface|null
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @param $token
     * @param null $type
     * @return ApiUser|null
     */
    public static function findIdentityByAccessTokenDefault($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * @param mixed $privateKey
     * @param null $type
     * @return ApiUser|array|\yii\db\ActiveRecord|IdentityInterface|null
     * @throws \yii\web\HttpException
     */
    public static function findIdentityByAccessToken($privateKey, $type = null)
    {
        $controllerAction = Yii::$app->controller->id . '/' . Yii::$app->controller->action->id;
        $method = Yii::$app->request->method;
        $error = '';

        //Retrieve API user from api_users table by received public key (within URL)
        $publicKey = Yii::$app->request->get('pubkey');
        $apiUser = ApiUser::find()->joinWith('relatedToken')->where('public_key = :public_key', [':public_key' => $publicKey])->one();
        //Validate API user
        if ($error === '' && !$apiUser) {
            $error = 'User not found';
        }

        //Validate if the user (user type user) is active. If not, perform logout actions
        if ($error === '' && $apiUser->user && (int)$apiUser->user->active === 0 && !in_array($controllerAction, ['tools/ping', 'site/logout'])) {
            $error = 'Account is inactive, contact your manager';

            $model = new Site();
            $model->scenario = Site::SCENARIO_LOGOUT;
            $model->load(['public_key' => $publicKey], '');
            $model->logout();
        }

        //Validate received private key (within Authorization header).
        if ($error === '' && Yii::$app->params['authorization']['validateHttpHeader'] && !in_array($controllerAction, Yii::$app->params['authorization']['authenticateWithoutHeader'])) {
            $generatedKeyStatus = ApiGeneratedKey::checkKey($privateKey);

            if ($apiUser->private_key != $privateKey && $generatedKeyStatus !== true) {
                $error = 'API private generated key is invalid. Received key: "' . $privateKey . '".';
                if ($generatedKeyStatus !== true) {
                    $error .= ' Status: "' . $generatedKeyStatus . '"';
                }
            }
        }

        if ($error === '') {
            $apiUserPublicKey = ApiToken::find()->where('api_user_id = :api_user_id AND public_key = :public_key', [':public_key' => $publicKey, ':api_user_id' => $apiUser->id])->one();
            if (!$apiUserPublicKey->validate('date_expire')) {
                $error = $apiUserPublicKey->getFirstError('date_expire');
            }
        }

        //Throw error if user not valid
        if ($error !== '') {
            throw new \yii\web\HttpException('401', Utils::errorMessage(401, $error));
        }

        Yii::$app->params['user']['id'] = $apiUser->user_id;
        Yii::$app->params['user']['type'] = $apiUser->user_type;
        Yii::$app->params['user']['ip'] = Yii::$app->request->getUserIP();

        $user = null; //will be used later

        //Check for received session page Url and timezone
        $params = array_merge(Yii::$app->request->bodyParams, Yii::$app->request->queryParams);
        if (Yii::$app->params['user']['type'] === 'user') {
            $user = User::findOne(Yii::$app->params['user']['id']);
            if ($user) {
                $sessionFields = [];
                if (!empty($params['session_page_url'])) {
                    $user->last_session_url = $params['session_page_url'];
                    $sessionFields[] = 'last_session_url';
                }
                if (!empty($params['session_timezone'])) {
                    $user->last_session_timezone = $params['session_timezone'];
                    $sessionFields[] = 'last_session_timezone';
                }
                if (!empty($sessionFields)) {
                    $user->last_session = new Expression('NOW()');
                    $sessionFields[] = 'last_session';
                    $user->save(false, $sessionFields);
                }
            }
        }

        //Saving user request. Need to perform number of request check (throttling).
        $apiRequestModel = new ApiRequest();
        $apiRequestModel->user_id = Yii::$app->params['user']['id'];
        $apiRequestModel->user_type = Yii::$app->params['user']['type'];
        if (!$apiRequestModel->save()) {
            throw new \yii\web\HttpException('403', Utils::errorMessage(403, implode("\n", $apiRequestModel->getFirstErrors())));
        }

        if (Yii::$app->params['user']['type'] !== 'user') {
            throw new \yii\web\HttpException('403', Utils::errorMessage(403, 'this user type is not allowed to perform API actions'));
        }

        //Validating user account (for `user` user type only)
        if (Yii::$app->params['user']['type'] === 'user') {
            if (!$user) {
                throw new \yii\web\HttpException('403', Utils::errorMessage(403, 'user not found'));
            }

            if ($user->relatedRole) {
                Yii::$app->params['user']['role'] = strtolower($user->relatedRole->name);
            }
            Yii::$app->params['user']['timezone'] = $user->timezone;

            $params = array_merge(Yii::$app->request->queryParams, Yii::$app->request->bodyParams);

            //Validate this endpoint can be accessed without account id
            if (
                !in_array($method . ',' . $controllerAction, Yii::$app->params['authorization']['authenticateWithoutAccount']) &&
                empty($params['account_uid'])
            ) {
                $error = 'account unique id not received';
            }

            //Validate user access to the received account id
            if ($error == '' && !empty($params['account_uid'])) {

                //Validate, whether account exists
                $account = Account::findByUniqueId($params['account_uid']);
                if (!$account) {
                    $error = 'account ID not found';
                }

                //Validate user role againsta received account id (administrator can access all accounts of the company of the user)
                if ($error === '') {
                    //validate administrator access
                    if (in_array('administrator', $user->roles)) {
                        $model = Account::find()->where('accounts.unique_id = :unique_id AND accounts.company_id = :company_id', ['unique_id' => $account->unique_id, 'company_id' => $user->company_id])->one();
                        if (!$model) {
                            $error = 'you are not allowed to access this account';
                        }
                    } //validate none administrator access (through user_accounts table)
                    else {
                        $model = UserAccount::find()->where(['user_id' => $user->id, 'account_id' => $account->id])->one();
                        if (!$model) {
                            $error = 'you are not allowed to access this account';
                        }
                    }
                }

                if ($error != '') {
                    throw new \yii\web\HttpException('403', Utils::errorMessage(403, $error));
                }
                $user->updateHistory($account->id);
            }

            if ($error != '') {
                throw new \yii\web\HttpException('403', Utils::errorMessage(403, $error));
            }

            $account = $user->lastAccount;
            if ($account) {
                if ((int)$account->active === 0) {
                    throw new \yii\web\HttpException('403', Utils::errorMessage(403, 'this account is suspended'));
                }

                Yii::$app->params['user']['accountId'] = $account->id;
            }
        }

        return $apiUser;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['private_key'], 'string', 'max' => 10],
            [['private_key'], 'default', 'value' => function () {
                return Utils::randomizer(['purpose' => 'apiPrivateKey']);
            }, 'when' => function ($model) {
                return $model->isNewRecord;
            }],
            [['create_date'], 'default', 'value' => new Expression("NOW()"), 'when' => function ($model) {
                return $model->isNewRecord;
            }],
        ];
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     * @throws \Exception
     */
    public function afterSave($insert, $changedAttributes)
    {
        //New user
        if ($insert) {
            $auth = Yii::$app->authManager;
            $role = $auth->getRole($this->user_type);
            $auth->assign($role, $this->id);

            $apiToken = new ApiToken();
            $apiToken->api_user_id = $this->id;
            $apiToken->save();
        }
    }

    /**
     * @return int|mixed|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $authKey
     * @return bool
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * @return mixed|string
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelatedToken()
    {
        return $this->hasMany(ApiToken::className(), ['api_user_id' => 'id']);
    }

    /**
     * @return ApiToken[]|UserRole[]|array|\yii\db\ActiveRecord[]
     */
    public function getActiveTokens()
    {
        return ApiToken::find()->where(['and', "api_user_id=" . (int)$this->id, "date_expire>=NOW()"])->orderBy(['date_expire' => SORT_ASC])->all();
    }

    /**
     * @return mixed
     */
    public function getActiveKeys()
    {
        return ApiGeneratedKey::find()->where(['and', "api_user_id=" . (int)$this->id, "expire_date>=NOW()"])->orderBy(['expire_date' => SORT_DESC])->all();
    }

    /**
     * @return User|bool|null
     */
    public function getUser()
    {
        if ($this->user_type !== 'user') {
            return false;
        }
        return User::findOne($this->user_id);
    }

}