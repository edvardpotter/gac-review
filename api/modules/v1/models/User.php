<?php

namespace app\api\modules\v1\models;

use app\api\modules\v1\components\Crypto;
use app\api\modules\v1\models\common\MyActiveRecord;
use app\components\Utils;
use app\components\utils\TimeHelper;
use Yii;
use yii\db\Query;


/**
 * Class User
 * @package app\api\modules\v1\models
 */
class User extends MyActiveRecord
{

    /**
     *
     */
    const SCENARIO_CREATE = 'create';
    /**
     *
     */
    const SCENARIO_UPDATE = 'update';
    /**
     *
     */
    const SCENARIO_CREATE_CLIENT_LOGIN = 'create_client_login';

    /**
     * @var
     */
    public $user_role;
    /**
     * @var
     */
    public $name;
    /**
     * @var string
     */
    public $defaultTimezone = '-10:00'; //PST
    /**
     * @var
     */
    public $current_password;
    /**
     * @var
     */
    public $old_password;
    /**
     * @var
     */
    public $old_email;
    /**
     * @var
     */
    public $currentUserId;
    /**
     * @var
     */
    public $currentAccountId;
    /**
     * @var string
     */
    public $customError = ''; //Used to store custom errors
    /**
     * @var
     */
    public $prospect;
    /**
     * @var
     */
    public $db;
    /**
     * @var string
     */
    private $paySource = 'company';

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @param $email
     * @return User|bool|null
     */
    public static function checkExists($email)
    {
        $user = static::findOne(['email' => $email]);
        if (!$user) {
            return false;
        }
        return $user;
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->db = Utils::getDb();
        $this->on(self::EVENT_AFTER_FIND, [$this, 'runAfterFind']);
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['update'] = ['user_role', 'first_name', 'last_name', 'email', 'password', 'current_password', 'phone', 'phone_internal', 'position', 'timezone', 'notes', 'avatar_url', 'currentUserId'];
        $scenarios['reset_password'] = ['password'];
        $scenarios['create_client_login'] = ['unique_id', 'first_name', 'last_name', 'user_role', 'email', 'phone', 'position', 'notes', 'currentAccountId', 'timezone'];
        return $scenarios;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['user_role', 'first_name', 'last_name', 'email', 'company_id', 'currentUserId'], 'required', 'on' => self::SCENARIO_CREATE],
            [['first_name', 'user_role', 'email', ' timezone'], 'required', 'on' => self::SCENARIO_CREATE_CLIENT_LOGIN],

            [['user_role_id', 'active'], 'integer'],
            [['currentUserId', 'currentAccountId'], 'safe'],
            [['notes', 'log'], 'string'],
            [['unique_id', 'last_session_timezone', 'timezone'], 'string', 'max' => 10],
            [['first_name', 'last_name', 'position'], 'string', 'max' => 50],
            [['password'], 'string', 'max' => 100],
            [['email'], 'string', 'max' => 50],
            [['phone', 'phone_internal', 'last_session_url', 'avatar_url'], 'string', 'max' => 255],
            [['ip'], 'string', 'max' => 15],
            [['user_role'], 'exist', 'targetClass' => UserRole::className(), 'targetAttribute' => ['user_role' => 'name']],
            [['user_role'], 'in', 'range' => ['SelfClient', 'BasicClient'], 'on' => self::SCENARIO_CREATE_CLIENT_LOGIN],
            [['currentUserId'], 'exist', 'targetClass' => User::className(), 'targetAttribute' => ['currentUserId' => 'id']],
            ['password', 'match', 'pattern' => Yii::$app->params['patterns']['password'], 'message' => '{attribute} must be at least 8 characters long'],
            ['timezone', 'match', 'pattern' => Yii::$app->params['patterns']['timezone'], 'message' => 'Bad format for {attribute}'],

            [['email'], 'email'],
            ['email', 'unique', 'message' => 'User with the same email already exist'],
            [['unique_id'], 'default', 'value' => function () {
                return Utils::randomizer(['purpose' => 'object']);
            }, 'when' => function ($model) {
                return $model->isNewRecord;
            }],
            [['active'], 'default', 'value' => 1, 'when' => function ($model) {
                return $model->isNewRecord;
            }],
            [['create_date'], 'default', 'value' => Yii::$app->params['now'], 'when' => function ($model) {
                return $model->isNewRecord;
            }],
            [['ip'], 'default', 'value' => Yii::$app->request->getUserIP(), 'when' => function ($model) {
                return $model->isNewRecord;
            }],

        ];
    }

    /**
     *
     */
    public function runAfterFind()
    {
        $this->old_password = $this->password;
        $this->old_email = $this->email;
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {

        if (parent::beforeValidate()) {
            $user = null;

            if ($this->currentUserId) {
                $user = static::findOne($this->currentUserId);
                if ($user) {
                    $this->company_id = $user->company_id;
                }
            }

            if (!empty($this->user_role)) {
                $model = UserRole::findOne(['name' => $this->user_role]);
                if ($model) {

                    if ($user && !$user->validateRole($this->user_role)) {
                        $this->addError($this->user_role, 'Action not allowed for the user role');
                        return false;
                    }

                    if ($this->isNewRecord && $model->id > 5 && $this->scenario != self::SCENARIO_CREATE_CLIENT_LOGIN) {
                        $this->addError($this->user_role, 'Action not allowed for the user role');
                        return false;
                    }

                    $this->user_role_id = $model->id;
                }
            }

            //Validate current password (if received)
            if (!empty($this->current_password)) {
                if (!self::validatePassword($this->current_password, $this->old_password)) {
                    $this->addError($this->current_password, 'Current password do not match');
                }
                if (empty($this->password)) {
                    $this->addError($this->password, 'Password must be at least 8 characters long');
                }
            }

            return true;
        }
        return false;
    }

    /**
     * @param $roleName
     * @return bool
     */
    public function validateRole($roleName)
    {
        $roleModel = UserRole::findOne(['name' => $roleName]);
        if (!$roleModel) {
            return false;
        }

        if ($this->user_role_id > $roleModel->id || (int)$roleModel->id > 5) {
            return false;
        }
        return true;
    }

    /**
     * @param $password
     * @param $hash
     * @return mixed
     */
    public static function validatePassword($password, $hash)
    {
        return Crypto::verifyHash($password, $hash);
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        //Set company_id for the new user based on account for endpoint accounts/:account_uid/client_login
        if ($this->currentAccountId && $this->scenario === self::SCENARIO_CREATE_CLIENT_LOGIN) {
            $model = Account::findOne($this->currentAccountId);
            if ($model) {
                $this->company_id = $model->company_id;
            }
        }

        //Do not allow operations on the users outside your company
        if ($this->scenario === SELF::SCENARIO_UPDATE) {
            if ($this->relatedCompany && !in_array($this->currentUserId, $this->relatedCompany->relatedUserIds)) {
                $this->addError($this->id, 'You are not allowed to perform actions on this user');
                return false;
            }
        }

        //Update users.unique_id from prospects (if any)
        if ($this->scenario === SELF::SCENARIO_CREATE) {
            $prospect = Prospect::findOne(['email' => $this->email]);
            if ($prospect) {
                $this->unique_id = $prospect->unique_id;
                $this->prospect = $prospect;
            }
        }

        //Encrypt password on user create/update
        if (in_array($this->scenario, [SELF::SCENARIO_CREATE, SELF::SCENARIO_UPDATE])) {
            if (!empty($this->password)) {
                $this->password = Crypto::makeHash($this->password);
            } else {
                $this->password = $this->old_password;
            }
        }

        return true;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {

        //is new record
        if ($insert) {
            $apiUser = new ApiUser();
            $apiUser->user_id = $this->id;
            $apiUser->user_type = 'user';
            $apiUser->protected = 1;
            $apiUser->save();

            if ($this->prospect) {
                $this->prospect->unique_id = Utils::randomizer(['purpose' => 'object']);
                $this->prospect->active = 0;
                $this->prospect->save();
            }

            if ($this->currentAccountId && $this->scenario === self::SCENARIO_CREATE_CLIENT_LOGIN) {
                $model = new UserAccount();
                $model->user_id = $this->id;
                $model->account_id = $this->currentAccountId;
                $model->save();
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @param array $accounts
     * @return bool
     */
    public function assignAccounts($accounts = [])
    {
        if (!is_array($accounts) || (is_array($accounts) && empty($accounts))) {
            return false;
        }

        foreach ($accounts as $key => $account_id) {
            if (UserAccount::findOne(['user_id' => $this->id, 'account_id' => $account_id])) {
                $account = Account::findOne($account_id);
                $this->customError = 'Account with ID "' . $account->unique_id . '" is already assigned to user "' . $this->unique_id . '"';
                return false;
            }
        }

        foreach ($accounts as $key => $account_id) {
            $userAccount = new UserAccount();
            $userAccount->user_id = $this->id;
            $userAccount->account_id = $account_id;
            $userAccount->save();
        }

        return true;
    }

    /**
     * @param array $accounts
     * @return bool
     */
    public function removeAccounts($accounts = [])
    {
        if (!is_array($accounts) || (is_array($accounts) && empty($accounts))) {
            return false;
        }

        UserAccount::deleteAll(['and', 'user_id = :user_id', ['IN', 'account_id', $accounts]], [':user_id' => $this->id]);
        return true;
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'unique_id' => 'Unique ID',
            'company_id' => 'Company ID',
            'user_role_id' => 'User Role ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'password' => 'Password',
            'email' => 'Email',
            'phone' => 'Phone',
            'phone_internal' => 'Phone Internal',
            'position' => 'Position',
            'active' => 'Active',
            'ip' => 'Ip',
            'create_date' => 'Create Date',
            'last_session' => 'Last Session',
            'last_session_url' => 'Last Session Url',
            'last_session_timezone' => 'Last Session Timezone',
            'timezone' => 'Timezone',
            'notes' => 'Notes',
            'log' => 'Log',
            'avatar_url' => 'Avatar Url',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelatedCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return $this->first_name . ' ' . mb_substr($this->last_name, 0, 1) . '.';
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelatedRole()
    {
        return $this->hasOne(UserRole::className(), ['id' => 'user_role_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getRelatedAccountsViaUserAccountsTable()
    {
        return $this->hasMany(Account::className(), ['id' => 'account_id'])->where(['active' => 1])->viaTable('user_accounts', ['user_id' => 'id']);
    }

    /**
     * @return mixed|null
     */
    public function getRelatedAccounts()
    {
        if (!$this->relatedRole) {
            return null;
        }

        if ((int)$this->user_role_id === 1) {
            $accounts = Account::find()->where(['company_id' => $this->company_id, 'active' => 1])->all();
        } else {
            $accounts = $this->relatedAccountsViaUserAccountsTable;
        }
        return $accounts;
    }


    //Get related user account

    /**
     * @return array
     */
    public function getRelatedAccountIds()
    {
        $out = [];
        if (!$this->relatedAccounts) {
            return $out;
        }
        foreach ($this->relatedAccounts as $key => $account) {
            if (!in_array($account->id, $out)) {
                $out[] = (int)$account->id;
            }
        }
        return $out;
    }

    /**
     * @return ApiUser|null
     */
    public function getApiUser()
    {
        return ApiUser::findOne(['user_id' => $this->id, 'user_type' => 'user']);
    }

    /**
     * @return |null
     */
    public function getLastAccount()
    {
        //Retrieve last used account and send it back
        $lastAccount = UserAccountHistory::getLastAccount($this->id);
        if ($lastAccount) {
            return $lastAccount->account;
        }

        //Retrieve first assigned account (sorted ASC by accounts.account_name)
        $query = new Query();
        $accounts = $query->select(['a.id'])->from('accounts a')->leftJoin('user_accounts ua', 'ua.account_id=a.id')->where(['user_id' => $this->id])->orderBy(['a.account_name' => SORT_ASC])->all();
        if (!empty($accounts)) {
            $accountId = $accounts[0]['id'];
            $this->updateHistory($accountId);
            return Account::findOne($accountId);
        }

        return null;
    }

    /**
     * @param $accountId
     * @param array $options
     */
    public function updateHistory($accountId, $options = [])
    {
        $defaults = ['logged_as_admin' => 0];
        $opts = array_merge($defaults, $options);
        $accountHistory = new UserAccountHistory();
        $accountHistory->user_id = $this->id;
        $accountHistory->account_id = $accountId;
        $accountHistory->logged_as_admin = (int)$opts['logged_as_admin'];
        $accountHistory->save();
    }

    /**
     * @return false|string|null
     * @throws \yii\db\Exception
     */
    public function getBalance()
    {
        $db = Utils::getDb();

        $filter = "account_id=" . (int)$this->lastAccount->id . " AND own_balance=1";
        if ($this->paySource === 'company') {
            $filter = "company_id=" . (int)$this->relatedCompany->id . " AND own_balance=0";
        }

        $sql = "SELECT
                    IFNULL((SELECT SUM(amount) FROM client_payments WHERE " . $filter . " AND payment_type='DEBIT' AND payment_success=1 ),0) -
                    IFNULL((SELECT SUM(amount) FROM client_payments WHERE " . $filter . " AND payment_type='CREDIT' AND payment_success=1),0) AS balance
    ";

        return $db->createCommand($sql)->queryScalar();
    }

    /**
     * @return array
     */
    public function getPayDetails()
    {
        if ($this->lastAccount && (int)$this->lastAccount->own_balance === 1) {
            $this->paySource = 'account';
        }

        //Override own_balance if manualy defined
        $case = $this->paySource;
        $out = [
            'paySource' => $this->paySource,
            'uniqueId' => '',                          //used in payments (Braintree etc.)
            'currencyId' => '',
            'currencyCode' => 'USD',
            'currencyText' => 'US Dollar',
            'currencyShort' => '$',
            'currencyRate' => 1,
            'hasVat' => false,
            'vatPercent' => 0,
            'stripeId' => '',
            'companyId' => 0,
            'ownBalance' => 0,
            'email' => '',                          //used in payments (Braintree etc.)
            'company' => ''                           //used in payments (Braintree etc.)
        ];

        $currency = false;
        $country = false;

        if ($case === 'account' && $this->lastAccount) {
            $currency = $this->lastAccount->relatedCurrency;
            $country = $this->lastAccount->relatedBillingCountry;
            $out['stripeId'] = (string)$this->lastAccount->stripe_id;
            $out['ownBalance'] = 1;
            $out['email'] = (string)$this->lastAccount->billing_email;
            $out['uniqueId'] = (string)$this->lastAccount->unique_id;
            $out['company'] = (string)$this->lastAccount->account_name;
        }

        if ($case === 'company' && $this->relatedCompany) {
            $currency = $this->relatedCompany->relatedCurrency;
            $country = $this->relatedCompany->country;
            $out['stripeId'] = (string)$this->relatedCompany->stripe_id;
            $out['companyId'] = (int)$this->relatedCompany->id;
            $out['email'] = (string)$this->relatedCompany->billing_email;
            $out['uniqueId'] = (string)$this->relatedCompany->unique_id;
            $out['company'] = (string)$this->relatedCompany->company_name;
        }

        if ($currency) {
            $out['currencyId'] = $currency->id;
            $out['currencyCode'] = $currency->code;
            $out['currencyText'] = $currency->full_name;
            $out['currencyRate'] = $currency->rate;
            $out['currencyShort'] = $currency->short_name;
        }

        if ($country) {
            $out['countryId'] = (int)$country->id;
            $out['countryCode'] = (int)$country->country_code;
        }

        return $out;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        $roles = [];
        UserRole::getRoles((int)$this->user_role_id, $roles);
        return $roles;
    }

    /**
     * @return int
     */
    public function getOrderCount()
    {
        if (!$this->lastAccount) {
            return 0;
        }
        return AccountOrder::find()->where(['account_id' => $this->lastAccount->id])->count();
    }

    /**
     * @return bool
     */
    public function actionDelete()
    {
        $currentUser = static::findOne($this->currentUserId);
        $userIds = [];
        if ($currentUser && $currentUser->relatedCompany) {
            $userIds = $currentUser->relatedCompany->relatedUserIds;
        }
        if (!in_array($this->id, $userIds)) {
            $this->customError = 'You are not allowed to perform actions on this user';
            return false;
        }
        $this->active = 0;
        if (!$this->save()) {
            $this->customError = implode("\n", $this->getFirstErrors());
            return false;
        }
        return true;
    }

    /**
     * @param array $options
     * @return array
     * @throws \yii\db\Exception
     */
    public function getPromocodes($options = [])
    {
        $defaults = [
            'active' => [1],
            'order' => 'cp.id',
            'bundlesOnly' => false,
            'companyId' => 0,
            'currencyId' => ''
        ];

        $opts = array_merge($defaults, $options);

        $out = [
            'max_discount_value' => 0,
            'max_discount_id' => '',
            'max_discount_code' => '',
            'max_discount_name' => '',
            'balance' => 0,
            'list' => []
        ];

        $db = Utils::getDb();
        $bundles = ConfigBundle::getBundlesByCode(['all' => true, 'currency_id' => $opts['currencyId']]);

        if ((int)$opts['companyId'] === 0) {
            $filter = " AND cp.account_id=" . (int)$this->lastAccount->id . " AND cp.own_balance=1";
        } else {
            $filter = "AND cp.account_id IN (SELECT ac.id AS account_id FROM accounts ac WHERE ac.company_id=" . (int)$this->company_id . ")";
        }

        $sql = "SELECT
				cp.promocode_id,
				cp.bundle_amount,
				cp.balance,
				p.code,
				p.client_discount,
				p.copywriter_discount
			FROM client_promocodes cp
			LEFT JOIN promocodes p ON cp.promocode_id = p.id
			WHERE cp.active IN (" . implode(',', $opts['active']) . ") " . $filter . "
			ORDER BY " . $opts['order'];

        $command = $db->createCommand($sql);

        foreach ($command->queryAll() as $row) {
            $bundle = ['name' => 'Custom'];
            if (!isset($bundles[$row['promocode_id']]) && $opts['bundlesOnly']) {
                break;
            }
            if (isset($bundles[$row['promocode_id']])) {
                $bundle = array_merge($bundle, $bundles[$row['promocode_id']]);
            }

            if ($out['max_discount_value'] < (float)$row['client_discount']) {
                $out['max_discount_value'] = (float)$row['client_discount'];
                $out['max_discount_id'] = $row['promocode_id'];
                $out['max_discount_code'] = $row['code'];
                $out['max_discount_name'] = $bundle['name'];
            }

            $out['balance'] += $row['balance'];
            $row['name'] = $bundle['name'];
            $out['list'][] = $row;
        }

        return $out;
    }

    /**
     * @return array|false|mixed
     */
    public function fields()
    {
        $action = Yii::$app->controller->action->id; //Get current controller/action
        $controllerAction = Yii::$app->controller->id . '/' . Yii::$app->controller->action->id;
        $fields = parent::fields(); //Retrieve default fields
        $actionFields = [];

        $actionFields['keys'] = [
            'user_id' => 'unique_id',
            'user_role' => function () {
                return $this->relatedRole ? $this->relatedRole->name : null;
            },
            'private_key' => function () {
                return $this->apiUser ? $this->apiUser->private_key : null;
            },
            'public_key' => function () {
                return ($this->apiUser && $this->apiUser->relatedToken) ? ApiToken::find()->where(['api_user_id' => $this->apiUser->id])->all() : null;
            },
        ];

        $actionFields['details'] = [
            'user_id' => 'unique_id',
            'user_role' => function () {
                return $this->relatedRole ? $this->relatedRole->name : null;
            },
            'first_name',
            'last_name',
            'avatar' => 'avatar_url',
            'support_first_name' => function () {
                return $this->relatedCompany && $this->relatedCompany->am ? $this->relatedCompany->am->firstName : null;
            },
            'support_last_name' => function () {
                return $this->relatedCompany && $this->relatedCompany->am ? $this->relatedCompany->am->lastName : null;
            },
            'support_avatar' => function () {
                return $this->relatedCompany && $this->relatedCompany->am ? $this->relatedCompany->am->avatar_url : null;
            },
            'support_phone' => function () {
                return $this->relatedCompany ? CountryPhone::getPhone($this->relatedCompany->country_id) : null;
            },
            'support_extension' => function () {
                return $this->relatedCompany && $this->relatedCompany->am ? $this->relatedCompany->am->phone_ext : null;
            },
            'accounts' => function () {
                return $this->relatedAccounts ? $this->relatedAccounts : null;
            },
        ];

        $actionFields['index'] =
        $actionFields['create'] =
        $actionFields['update'] =
        $actionFields['add-account'] =
        $actionFields['delete-account'] =
        $actionFields['accounts/create-client-login'] =
            [
                'user_id' => 'unique_id',
                'user_role' => function () {
                    return $this->relatedRole ? $this->relatedRole->name : null;
                },
                'first_name',
                'last_name',
                'position',
                'email',
                'phone',
                'create_date' => function () {
                    return TimeHelper::convertUTC($this->create_date, ['timezone' => 'loggedUser', 'direction' => 'fromUTC']);
                },
                'last_session' => function () {
                    return $this->last_session ? TimeHelper::convertUTC($this->last_session, ['timezone' => 'loggedUser', 'direction' => 'fromUTC']) : null;
                },
                'timezone',
                'notes',
                'accounts' => function () {
                    return $this->relatedAccounts ? $this->relatedAccounts : null;
                },
                'avatar_url'
            ];

        if (isset($actionFields[$action])) {
            return $actionFields[$action];
        }
        if (isset($actionFields[$controllerAction])) {
            return $actionFields[$controllerAction];
        }
        return $fields;
    }

    /**
     * @param $password
     */
    public function setPassword($password)
    {
        $this->password = Crypto::makeHash($password);
        $this->save(false, ['password']);
    }
}
