<?php

namespace app\api\modules\v1\models;

use app\api\modules\v1\models\common\MyActiveRecord;
use Yii;
use yii\db\Expression;
use yii\helpers\Url;

/**
 * Class ApiRequest
 * @package app\api\modules\v1\models
 */
class ApiRequest extends MyActiveRecord
{

    /**
     * @var int
     */
    public $limit = 1;

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'api_requests';
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->limit = Yii::$app->params['throttlingLimit'];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {

        if (!$this->history) {
            $this->addError('created_at', 'Too many requests');
            return false;
        }

        return true;
    }


    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['created_at'], 'safe'],
            [['method'], 'string', 'max' => 10],
            [['method'], 'default', 'value' => function () {
                return $this->requestMethod;
            }, 'when' => function ($model) {
                return $model->isNewRecord;
            }],
            [['endpoint'], 'default', 'value' => function () {
                return $this->requestEndpoint;
            }, 'when' => function ($model) {
                return $model->isNewRecord;
            }],
            [['created_at'], 'default', 'value' => Yii::$app->params['now'], 'when' => function ($model) {
                return $model->isNewRecord;
            }],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'session' => 'Session',
            'created_at' => 'Created At',
            'data' => 'Data',
        ];
    }

    /**
     * @return mixed|string
     */
    public function getRequestMethod()
    {
        return Yii::$app->request->method;
    }

    /**
     * @return string
     */
    public function getRequestEndpoint()
    {
        return Url::current();
    }


    /**
     * @return bool
     */
    public function getHistory()
    {

        //Delete history data older than 14 days
        static::deleteAll('user_id = :user_id AND user_type = :user_type AND created_at < DATE_ADD(NOW(), INTERVAL -14 DAY)', ['user_id' => $this->user_id, 'user_type' => $this->user_type]);

        $total = static::find()
            ->where(['user_id' => $this->user_id, 'user_type' => $this->user_type])
            ->andWhere(new Expression('created_at BETWEEN DATE_ADD(NOW(), INTERVAL -1 MINUTE) AND NOW()'))
            ->count();

        if ((int)$total > $this->limit) {
            return false;
        }

        return true;
    }


}
