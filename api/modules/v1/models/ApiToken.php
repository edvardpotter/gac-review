<?php

namespace app\api\modules\v1\models;

use app\api\modules\v1\models\common\MyActiveRecord;
use app\components\Utils;
use Yii;

/**
 * Class ApiToken
 * @package app\api\modules\v1\models
 */
class ApiToken extends MyActiveRecord
{

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'api_tokens';
    }

    /**
     * @param $pubkey
     * @return ApiToken|null
     */
    public static function findByPubkey($pubkey)
    {
        return static::findOne(['public_key' => $pubkey]);
    }

    /**
     * @param $id
     * @param $type
     * @return ApiToken[]|UserRole[]|array|\yii\db\ActiveRecord[]
     */
    public static function findByUserIdAndType($id, $type)
    {
        return static::find()->joinWith('relatedApiUser u')->where('u.user_id = :user_id AND u.user_type = :user_type', ['user_id' => $id, 'user_type' => $type])->all();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['public_key'], 'default', 'value' => function () {
                return Utils::randomizer(['purpose' => 'apiPublicKey']);
            }, 'when' => function ($model) {
                return $model->isNewRecord;
            }],
            [['date_expire'], 'default', 'value' => '2030-01-01', 'when' => function ($model) {
                return $model->isNewRecord;
            }],

            [['public_key', 'api_user_id', 'date_expire'], 'required'],
            [['api_user_id'], 'integer'],
            ['date_expire', 'date', 'format' => 'yyyy-M-d'],
            ['date_expire', function ($attribute, $params, $validator) {
                if (strtotime($this->$attribute) < strtotime(Yii::$app->params['now'])) {
                    $this->addError($attribute, 'The token has been expired.');
                }
            }],
            [['public_key'], 'string', 'max' => 50],
            [['callback_url'], 'string', 'max' => 255],
            [['public_key'], 'unique'],
            [['callback_url'], 'safe'],


        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'api_user_id' => 'User ID',
            'public_key' => 'Public key',
            'date_expire' => 'Date Expire',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelatedApiUser()
    {
        return $this->hasOne(ApiUser::className(), ['id' => 'api_user_id']);
    }

    /**
     * @return array|false|mixed
     */
    public function fields()
    {
        $controllerAction = Yii::$app->controller->id . '/' . Yii::$app->controller->action->id;
        $fields = [
            'isExpired' => function () {
                return strtotime($this->date_expire) < strtotime(Yii::$app->params['now']);
            },
        ];
        $actionFields = [];
        $actionFields['users/keys'] = ['public_key'];
        if (isset($actionFields[$controllerAction])) {
            return $actionFields[$controllerAction];
        }
        return $fields;
    }

}
