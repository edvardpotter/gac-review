<?php

namespace app\api\modules\v1\models\common;

use Yii;

/**
 * Class MyActiveRecord
 * @package app\api\modules\v1\models\common
 */
class MyActiveRecord extends \yii\db\ActiveRecord
{
    //Override ActiveRecord getDb funciton if active databse selected via sessions
    /**
     * @return mixed|\yii\db\Connection
     */
    public static function getDb()
    {
        $connection = Yii::$app->getDb();

        if (Yii::$app->params['is_stage']) {
            $session = Yii::$app->session;

            $file = Yii::getAlias('@app/db.cnf');
            if (@file_exists($file)) {
                $tmp = @file($file);
                $db_postfix = $tmp[0];
                $db_string = 'db_' . $db_postfix;
                if (isset(Yii::$app->{$db_string})) {
                    $connection = Yii::$app->{$db_string};
                }
            }

            if (isset($session['ACTIVE_DB'])) {
                $db_postfix = str_replace('getacopywriter', '', $session['ACTIVE_DB']);
                $db_string = 'db' . $db_postfix;
                if (isset(Yii::$app->{$db_string})) {
                    $connection = Yii::$app->{$db_string};
                }
            }

        }

        if (Yii::$app->params['dbname_ar'] === '') {
            preg_match("/dbname=([^;]*)/", $connection->dsn, $matches);
            if (isset($matches[1])) Yii::$app->params['dbname_ar'] = $matches[1];
        }

        return $connection;
    }
}