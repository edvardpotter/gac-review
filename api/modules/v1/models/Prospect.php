<?php

namespace app\api\modules\v1\models;

use app\api\modules\v1\models\common\MyActiveRecord;
use app\components\Utils;
use app\components\utils\IpHelper;
use Yii;

/**
 * Class Prospect
 * @package app\api\modules\v1\models
 */
class Prospect extends MyActiveRecord
{

    /**
     *
     */
    const SCENARIO_CREATE = 'create';
    /**
     *
     */
    const SCENARIO_UPDATE = 'update';

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'prospects';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['email'], 'required', 'on' => self::SCENARIO_CREATE],
            [['first_added'], 'safe'],
            [['country_id', 'user_agent_id', 'active'], 'integer'],
            [['unique_id'], 'string', 'max' => 10],
            [['email'], 'string', 'max' => 255],
            [['email'], 'email'],
            [['ip'], 'string', 'max' => 15],

            [['unique_id'], 'default', 'value' => function () {
                return Utils::randomizer(['purpose' => 'object']);
            }],
            [['first_added'], 'default', 'value' => Yii::$app->params['now']],
            [['ip'], 'default', 'value' => Yii::$app->request->getUserIP()],
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     * @throws \yii\db\Exception
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($this->scenario === SELF::SCENARIO_CREATE && static::find()->where(['email' => $this->email])->one()) {
            Utils::log('api_client', 'Prospect with "' . $this->email . '" already exists');
            return true;
        }

        if ((string)$this->country_id === '' && (string)$this->ip != '') {
            $results = IpHelper::getCountryByIP2Location($this->ip, ['output' => 'all']);
            if (!empty($results) && isset($results['country_id'])) {
                $this->country_id = $results['country_id'];
            }
        }

        if (!empty($_SERVER['HTTP_USER_AGENT'])) {
            $userAgent = UserAgent::find()->where('LOWER(agent_string) = :agent', [':agent' => strtolower($_SERVER['HTTP_USER_AGENT'])])->one();
            if (!$userAgent) {
                $model = new UserAgent();
                $model->agent_string = $_SERVER['HTTP_USER_AGENT'];
                if (!$model->save()) {
                    var_dump($model->getFirstErrors());
                }
                exit;
            }

            if ($userAgent) {
                $this->user_agent_id = $userAgent->id;
            }
        }

        return true;
    }


    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {

        //is new record
        if ($insert) {
            Yii::$app->segmentApi->track([
                'event' => 'Became Prospect',
                'properties' => ['Email' => $this->email, 'Signed up' => date("d.m.Y H:i", strtotime(Yii::$app->params['now']))],
                'userId' => $this->id,
                'userType' => 'prospect'
            ]);
        }

        parent::afterSave($insert, $changedAttributes);
    }


    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'unique_id' => 'Unique ID',
            'first_added' => 'First Added',
            'email' => 'Email',
            'ip' => 'Ip',
            'country_id' => 'Country ID',
            'user_agent_id' => 'User Agent ID',
            'active' => 'Active',
        ];
    }
}
