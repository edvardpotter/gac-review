<?php

namespace app\components;

use app\api\modules\v1\models\Account;
use app\api\modules\v1\models\Admin;
use app\api\modules\v1\models\Api;
use app\api\modules\v1\models\ApiGeneratedKey;
use app\api\modules\v1\models\ApiToken;
use app\api\modules\v1\models\ApiUser;
use app\api\modules\v1\models\Company;
use app\api\modules\v1\models\Config;
use app\api\modules\v1\models\Currency;
use app\api\modules\v1\models\File;
use app\api\modules\v1\models\Format;
use app\api\modules\v1\models\Lead;
use app\api\modules\v1\models\PasswordRestore;
use app\api\modules\v1\models\Project;
use app\api\modules\v1\models\ProjectType;
use app\api\modules\v1\models\Promocode;
use app\api\modules\v1\models\User;
use app\components\utils\ArrayHelper;
use Yii;
use yii\db\Query;


/**
 * Class Utils
 * @package app\components
 */
class Utils
{
    /**
     * @param $arr
     */
    public static function printr($arr)
    {
        echo '<pre>';
        print_r($arr);
        echo '</pre>';
    }

    /**
     * @param $arr
     */
    public static function vardump($arr)
    {
        echo '<pre>';
        var_dump($arr);
        echo '</pre>';
    }

    /**
     * @param $string
     * @param $substring
     * @return bool|string
     */
    public static function strafter($string, $substring)
    {
        $pos = strpos($string, $substring);
        if ($pos === false) {
            return $string;
        }
        return (substr($string, $pos + strlen($substring)));
    }

    /**
     * @param $string
     * @param $substring
     * @return bool|string
     */
    public static function strbefore($string, $substring)
    {
        $pos = strpos($string, $substring);
        if ($pos === false) {
            return $string;
        }
        return (substr($string, 0, $pos));
    }

    /**
     *
     */
    public static function hello()
    {
        echo 'Hello world!';
    }

    /**
     * @param $number
     * @param array $options
     * @return string
     */
    public static function formatNumber($number, $options = [])
    {
        return number_format($number, 2, '.', ',');
    }


    /**
     * @throws \yii\base\InvalidConfigException
     */
    public static function logRequests()
    {
        //Perform custom log of all received calls to API
        $pattern = '/^GetACopywriter\s+([\w\d]{3,20})$/';
        $timestamp = date("Y-m-d H:i:s", strtotime(Yii::$app->params['now']));
        $request = Yii::$app->request;
        $headers = $request->getHeaders();
        $privateKey = '';

        //Log received variables
        $vars = [
            'get' => $request->get(),
            'post' => $request->post(),
            'body' => $request->bodyParams,
            'headers' => $headers->toArray(),
        ];
        if (!empty($_SESSION)) {
            $vars['session'] = $_SESSION;
        }

        $logMsg = "\nRequest: " . $request->getUrl() . "\n";
        foreach ($vars as $key => $arr) {
            $logMsg .= strtoupper($key);
            if (empty($arr)) {
                $logMsg .= " (no)\n";
                continue;
            }
            $logMsg .= "\n" . ArrayHelper::implodeArray($arr) . "\n";
        }
        static::logApiVars($logMsg);


        if (!empty($headers) && isset($headers['authorization'])) {
            preg_match_all($pattern, $headers['authorization'], $matches);
            if (!empty($matches) && isset($matches[1]) && !empty($matches[1][0])) {
                $privateKey = $matches[1][0];
            }
        }

        $string = sprintf('%s %s %s %s %s',
            str_pad($timestamp, 20, ' ', STR_PAD_RIGHT),
            str_pad($request->getRemoteIP(), 17, ' ', STR_PAD_RIGHT),
            str_pad($request->getMethod(), 9, ' ', STR_PAD_RIGHT),
            str_pad('Private key: ' . $privateKey, 30, ' ', STR_PAD_RIGHT),
            $request->getUrl()
        );
        Utils::customLog('api_request', $string);
    }

    /**
     * @param $message
     * @param array $data
     */
    public static function logApiVars($message, $data = [])
    {
        $log = [];
        $log[] = $message;
        if (!empty($data)) {
            $log = array_merge($log, $data);
        }
        Yii::info(implode("\n", $log), 'api_vars');
    }

    /**
     * @param string $filename
     * @param string $data
     */
    public static function customLog($filename = '', $data = '')
    {
        $extension = '.log';
        if (empty($filename) || empty($data)) {
            return;
        }

        $file = Yii::getAlias('@logs') . '/' . $filename . $extension;
        file_put_contents($file, trim($data) . "\n", FILE_APPEND);
    }

    /**
     * @param array $options
     * @return string
     */
    public static function randomizer($options = [])
    {
        $defaults = [
            'length' => 10,
            'string' => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789',
            'purpose' => '',
            'callback' => null,
            'existing' => null       //Pass existing unique values (if any)
        ];
        $opts = array_merge($defaults, $options);

        $license = '';
        $invalid = true;

        if ($opts['purpose'] === 'apiPrivateKey') {
            $opts['callback'] = function ($license) {
                $result = ApiUser::find()->where('private_key = :private_key', [':private_key' => $license])->one();
                if (is_null($result)) {
                    return false;
                }
                return true;
            };
        }

        if ($opts['purpose'] === 'apiPublicKey') {
            $opts['string'] = strtolower($opts['string']);
            $opts['length'] = 15;
            $opts['callback'] = function ($license) {
                $result = ApiToken::find()->where('public_key = :public_key', [':public_key' => $license])->one();
                if (is_null($result)) {
                    return false;
                }
                return true;
            };
        }

        if ($opts['purpose'] === 'projectShareCode') {
            $opts['string'] = preg_replace('/[^\w]/', '', strtolower($opts['string']));
            $opts['length'] = 8;
            $opts['callback'] = function ($license) {
                $result = Project::find()->where('share_code = :code', [':code' => $license])->one();
                if (is_null($result)) {
                    return false;
                }
                return true;
            };
        }

        if ($opts['purpose'] === 'object') {
            $opts['callback'] = function ($license) {
                $result = User::find()->where('unique_id = :code', [':code' => $license])->one();
                if ($result) {
                    return true;
                }
                $result = Account::find()->where('unique_id = :code', [':code' => $license])->one();
                if ($result) {
                    return true;
                }
                $result = Company::find()->where('unique_id = :code', [':code' => $license])->one();

                if ($result) {
                    return true;
                }

                return false;
            };
        }

        if ($opts['purpose'] === 'accessToken') {
            $opts['callback'] = function ($license) {
                $result = ApiGeneratedKey::findOne(['access_token' => $license]);
                if ($result) {
                    return true;
                }
                return false;
            };
        }

        if ($opts['purpose'] === 'password_restore') {
            $opts['string'] = preg_replace('/[\d]+/u', '', strtolower($opts['string']));
            $opts['length'] = 16;
            $opts['callback'] = function ($license) {
                $result = PasswordRestore::find()->where(['secret' => $license])->one();
                if (is_null($result)) {
                    return false;
                }
                return true;
            };
        }

        if ($opts['purpose'] === 'file') {
            $opts['string'] = preg_replace('/[\d]+/u', '', strtolower($opts['string']));
            $opts['length'] = 5;
            $opts['callback'] = function ($license) {
                $result = File::find()->where('unique_id = :unique_id', [':unique_id' => $license])->one();
                if (is_null($result)) {
                    return false;
                }
                return true;
            };
        }


        $nr = 0;
        while ($invalid) {
            $license = '';
            mt_srand((double)microtime() * 1000000);
            for ($i = 1; $i <= (int)$opts['length']; $i++) {
                $license .= substr($opts['string'], mt_rand(0, strlen($opts['string']) - 1), 1);
            }

            if (!is_callable($opts['callback'])) {
                break;
            }

            $invalid = call_user_func_array($opts['callback'], ['license' => $license]);

            //Check against passed values (if any)
            if (is_array($opts['existing']) && !empty($opts['existing']) && array_search($license, $opts['existing']) !== false) {
                $invalid = true;
            }

            //Number of total retries
            if ($nr > 100) {
                $invalid = false;
            }
            $nr++;
        }

        return $license;
    }

    //Old name getProjectTypePrices

    /**
     * @param array $url
     * @return array
     */
    public static function googleFilesCheck($url = [])
    {
        $out = [
            'urlData' => [],
            'error' => ''
        ];

        $data = [
            'url' => !empty($url) ? $url : []
        ];
        $error = '';

        if ($error == '' && empty($data['url'])) {
            $error = 'Url not defined';
        }
        if ($error == '') {
            $urlList = $data['url'];

            foreach ($urlList as $key => $url) {
                $urlData = [
                    'title' => '',
                    'body' => '',
                    'bodyParams' => ['role' => '', 'itemtype' => ''],
                    'isOk' => '',
                    'error' => ''
                ];

                $url = base64_decode($url);
                $errorUrl = '';
                $curl = curl_init($url);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
                $rawOutput = curl_exec($curl);
                if (curl_error($curl)) $errorUrl = curl_error($curl);
                curl_close($curl);

                if ($errorUrl == '') {
                    //Replace all <script> tags as they might contain <body> tags
                    $html = preg_replace(Yii::$app->params['patterns']['scriptTag'], '', $rawOutput);

                    //Retrieve HTML title
                    preg_match_all(Yii::$app->params['patterns']['titleTag'], $html, $matches);
                    if (!empty($matches) && !empty($matches[1])) {
                        $urlData['title'] = strtolower($matches[1][0]);
                    }

                    $matches = [];
                    //Retrieve <body> tag params
                    preg_match_all(Yii::$app->params['patterns']['googleBody'], $html, $matches);
                    if (!empty($matches) && !empty($matches[1])) {
                        $urlData['body'] = preg_replace('/[\t\r\n ]+/si', ' ', $matches[1][0]);
                    }

                    $matches = [];
                    preg_match_all('/role="(.*?)"/si', $urlData['body'], $matches);
                    if (!empty($matches[1][0]) && !empty($matches[1])) {
                        $urlData['bodyParams']['role'] = $matches[1][0];
                    }

                    $matches = [];
                    preg_match_all('/itemtype="(.*?)"/si', $urlData['body'], $matches);
                    if (!empty($matches[1][0]) && !empty($matches[1])) {
                        $urlData['bodyParams']['itemtype'] = $matches[1][0];
                    }

                    if ($urlData['bodyParams']['role'] != '' && $urlData['bodyParams']['itemtype'] != '') {
                        $urlData['isOk'] = 'Matched main body params';
                    }
                    if ($urlData['isOk'] == '' && $urlData['title'] != 'moved temporarily') {
                        $urlData['isOk'] = 'Main body params not found, but title seems to be ok.';
                    }

                    if ($urlData['isOk'] == '') {
                        if ($errorUrl == '' && $urlData['title'] == 'moved temporarily') {
                            $errorUrl = 'Title has \'Moved Temporarily\' value';
                        }
                        if ($errorUrl == '' && $urlData['body'] == '') {
                            $errorUrl = 'Body tag has no data';
                        }
                    }
                }

                if ($errorUrl != '') {
                    $urlData['error'] = $errorUrl;
                    $error = 'Please ensure the Google Doc sharing is set to \'Anyone with the link\' or \'Public on the web\'';
                }

                $out['urlData'][] = $urlData;
            }
        }

        if ($error != '') {
            $out['error'] = $error;
        }

        return $out;
    }

    //Old name getProjectPrice

    /**
     * @param array $options
     * @return array
     * @throws \yii\db\Exception
     */
    public static function prices($options = [])
    {
        $defaults = ['currency_id' => '', 'filter' => ''];
        $opts = array_merge($defaults, $options);
        $out = $currency_ids = $priceTypes = $currencies = [];

        $tmp = Currency::find()->select('id, short_name')->all();

        foreach ($tmp as $key => $value) {
            $currency_ids[$value['id']] = 0;
            $currencies[$value['id']] = $value->short_name;
        }

        $sql = "SELECT
					pt.id,
					SUBSTRING_INDEX(pt.code,'-',1) AS code,
					SUBSTRING_INDEX(SUBSTRING_INDEX(pt.code,'-',2),'-',-1) AS code_type,
					SUBSTRING_INDEX(pt.code,'-',-1) AS is_custom,
					IFNULL(pt.words,0) AS words,
					pt.price_type,
					pt.level AS price_level,
					ptp.id AS price_id,
					ptp.currency_id,
					ptp.price,
					ptp.price_cw,
					ptp.editor,
					ptp.editor_revision,
					f.id AS format_id
			FROM
					project_types pt
			LEFT JOIN
					project_type_prices ptp ON pt.price_type=ptp.price_type AND pt.words=ptp.words
			LEFT JOIN formats f ON SUBSTRING_INDEX(pt.code,'-',1)=f.code
			" . (($opts['filter'] != '') ? 'WHERE ' . $opts['filter'] : '') . "
			ORDER BY
					pt.id
			";

        $rows = Yii::$app->db->createCommand($sql)->queryAll();
        foreach ($rows as $key => $row) {

            if (!in_array($row['price_type'], $priceTypes)) {
                $priceTypes[] = $row['price_type'];
            }

            //Setting up array element by using code (blog, article etc) as a key
            if (!isset($out[$row['code']])) {
                $out[$row['code']] = [
                    'id' => $row['format_id'],
                    'wordsAvl' => [],
                    'minWords' => ['standard' => 0, 'premium' => 0, 'all' => 0],
                    'maxWords' => ['standard' => 0, 'premium' => 0, 'all' => 0],
                    'minPrice' => ['standard' => $currency_ids, 'premium' => $currency_ids, 'all' => $currency_ids],
                    'maxPrice' => ['standard' => $currency_ids, 'premium' => $currency_ids, 'all' => $currency_ids],
                    'price_types' => [
                        'standard' => '',
                        'premium' => '',
                    ],
                    'prices' => [],
                    'unknown_prices' => []
                ];
            }

            if ((int)$row['price_level'] == 1 && $out[$row['code']]['price_types']['standard'] == '') {
                $out[$row['code']]['price_types']['standard'] = $row['price_type'];
            }
            if ((int)$row['price_level'] == 3 && $out[$row['code']]['price_types']['premium'] == '') {
                $out[$row['code']]['price_types']['premium'] = $row['price_type'];
            }

            //Setting up array element 'prices' for each major (blog, article etc) array item, using price_type (1,2,3,4,5,6) as a key
            if (!isset($out[$row['code']]['prices'][$row['price_type']])) {
                $out[$row['code']]['prices'][$row['price_type']] = [];
            }

            //Setting up array element for each price_type item, using list of currencies as a key
            if (!isset($out[$row['code']]['prices'][$row['price_type']][$row['words']])) {
                $out[$row['code']]['prices'][$row['price_type']][$row['words']] = $currency_ids;
            }

            //Setting up prices for each word count, depending on project type (blog, article etc), than price_type (1,2...6), than currency_id (1,2,3..)
            if ($row['is_custom'] != 'custom') {
                if ($row['currency_id'] != '') {
                    $out[$row['code']]['prices'][$row['price_type']][$row['words']][$row['currency_id']] = [
                        'price' => $row['price'],
                        'price_cw' => $row['price_cw'],
                        'editor' => $row['editor'],
                        'editor_revision' => $row['editor_revision'],
                    ];
                }
                if ($row['currency_id'] == '' && !in_array($row['words'], $out[$row['code']]['unknown_prices'])) {
                    $out[$row['code']]['unknown_prices'][] = $row['words'];
                }
            }

            //Calculating totals, min/max words for project type (blog, article etc) and its type (standard/premium/all),
            //min/max price for the project type (blog, article etc) and its type (standard/premium/all) and for each currency_id
            if ($row['currency_id'] != '') {
                if ($out[$row['code']]['minPrice']['all'][$row['currency_id']] > (int)$row['price'] || $out[$row['code']]['minPrice']['all'][$row['currency_id']] == 0) {
                    $out[$row['code']]['minPrice']['all'][$row['currency_id']] = (int)$row['price'];
                }
                if ($out[$row['code']]['maxPrice']['all'][$row['currency_id']] < (int)$row['price'] || $out[$row['code']]['maxPrice']['all'][$row['currency_id']] == 0) {
                    $out[$row['code']]['maxPrice']['all'][$row['currency_id']] = (int)$row['price'];
                }

                if ($row['code_type'] == 'standard') {
                    if ($out[$row['code']]['minPrice']['standard'][$row['currency_id']] > (int)$row['price'] || $out[$row['code']]['minPrice']['standard'][$row['currency_id']] == 0) {
                        $out[$row['code']]['minPrice']['standard'][$row['currency_id']] = (int)$row['price'];
                    }
                    if ($out[$row['code']]['maxPrice']['standard'][$row['currency_id']] < (int)$row['price'] || $out[$row['code']]['maxPrice']['standard'][$row['currency_id']] == 0) {
                        $out[$row['code']]['maxPrice']['standard'][$row['currency_id']] = (int)$row['price'];
                    }
                }

                if ($row['code_type'] == 'premium') {
                    if ($out[$row['code']]['minPrice']['premium'][$row['currency_id']] > (int)$row['price'] || $out[$row['code']]['minPrice']['premium'][$row['currency_id']] == 0) {
                        $out[$row['code']]['minPrice']['premium'][$row['currency_id']] = (int)$row['price'];
                    }
                    if ($out[$row['code']]['maxPrice']['premium'][$row['currency_id']] < (int)$row['price'] || $out[$row['code']]['maxPrice']['premium'][$row['currency_id']] == 0) {
                        $out[$row['code']]['maxPrice']['premium'][$row['currency_id']] = (int)$row['price'];
                    }
                }
            }

            if (($out[$row['code']]['minWords']['all'] > (int)$row['words'] || $out[$row['code']]['minWords']['all'] == 0) && (int)$row['words'] != 0) {
                $out[$row['code']]['minWords']['all'] = (int)$row['words'];
            }
            if (($out[$row['code']]['maxWords']['all'] < (int)$row['words'] || $out[$row['code']]['maxWords']['all'] == 0) && (int)$row['words'] != 0) {
                $out[$row['code']]['maxWords']['all'] = (int)$row['words'];
            }

            if ($row['code_type'] == 'standard') {
                if (($out[$row['code']]['minWords']['standard'] > (int)$row['words'] || $out[$row['code']]['minWords']['standard'] == 0) && (int)$row['words'] != 0) {
                    $out[$row['code']]['minWords']['standard'] = (int)$row['words'];
                }
                if (($out[$row['code']]['maxWords']['standard'] < (int)$row['words'] || $out[$row['code']]['maxWords']['standard'] == 0) && (int)$row['words'] != 0) {
                    $out[$row['code']]['maxWords']['standard'] = (int)$row['words'];
                }
            }

            if ($row['code_type'] == 'premium') {
                if (($out[$row['code']]['minWords']['premium'] > (int)$row['words'] || $out[$row['code']]['minWords']['premium'] == 0) && (int)$row['words'] != 0) {
                    $out[$row['code']]['minWords']['premium'] = (int)$row['words'];
                }
                if (($out[$row['code']]['maxWords']['premium'] < (int)$row['words'] || $out[$row['code']]['maxWords']['premium'] == 0) && (int)$row['words'] != 0) {
                    $out[$row['code']]['maxWords']['premium'] = (int)$row['words'];
                }
            }

            //Setting up available words count for the selected format
            if (!in_array((int)$row['words'], $out[$row['code']]['wordsAvl']) && (int)$row['words'] != 0) {
                $out[$row['code']]['wordsAvl'][] = (int)$row['words'];
            }
        }

        //Retrieving prices for the words more than defined (99999) for each price_type and currency_id
        $sql = "SELECT ptp.* FROM project_type_prices ptp WHERE ptp.words=99999 ORDER BY ptp.price_type";
        $rows = Yii::$app->db->createCommand($sql)->queryAll();
        $custom = [];
        foreach ($rows as $key => $row) {
            if (empty($custom)) {
                $custom = ['prices' => []];
            }
            if (!isset($custom['prices'][$row['price_type']])) {
                $custom['prices'][$row['price_type']] = $currency_ids;
            }
            if ($row['currency_id'] != '') {
                $custom['prices'][$row['price_type']][$row['currency_id']] = [
                    'price' => $row['price'],
                    'price_cw' => $row['price_cw'],
                    'editor' => $row['editor'],
                    'editor_revision' => $row['editor_revision'],
                ];
            }
        }

        if (!empty($custom)) {
            $out['custom_prices'] = $custom;
        }

        $tmp = [];
        $sql = "SELECT
				ptp.price_type,
				ptp.currency_id,
				ptp.words,
				ptp.price,
				ptp.price_cw,
				ptp.editor,
				ptp.editor_revision
			FROM project_type_prices ptp
			WHERE ptp.price_type IN (" . implode(',', $priceTypes) . ") AND ptp.words!=99999
			ORDER BY ptp.price_type, ptp.words
			";

        $rows = Yii::$app->db->createCommand($sql)->queryAll();
        foreach ($rows as $key => $row) {
            if (!isset($tmp[$row['price_type']])) {
                $tmp[$row['price_type']] = [];
            }
            if (!isset($tmp[$row['price_type']][$row['words']])) {
                $tmp[$row['price_type']][$row['words']] = [];
            }
            if (!isset($tmp[$row['price_type']][$row['words']][$row['currency_id']])) {
                $tmp[$row['price_type']][$row['words']][$row['currency_id']] = [
                    'price' => $row['price'],
                    'price_cw' => $row['price_cw'],
                    'editor' => $row['editor'],
                    'editor_revision' => $row['editor_revision']
                ];
            }
        }

        foreach ($out as $code => $value) {
            if ($code == 'custom_prices') {
                continue;
            } //Do not update prices for custom_prices

            foreach ($value['prices'] as $price_type => $prices) {
                if (isset($tmp[$price_type])) {
                    $tmpPrices = $tmp[$price_type];
                    $out[$code]['prices'][$price_type] = $tmpPrices;
                }
            }
        }

        $out['_currencies'] = $currencies;
        return $out;
    }

    /**
     * @param $prices
     * @param $project_type_id
     * @param $currency_id
     * @param array $options
     * @return array
     */
    public static function projectPrice($prices, $project_type_id, $currency_id, $options = [])
    {
        $defaults = ['words' => '', 'promocode_id' => ''];
        $opts = array_merge($defaults, $options);
        $out = [
            'error' => '',
            'words' => '',
            'prices' => [],
            'currency_id' => $currency_id,
            'currency' => []
        ];
        $error = '';

        $model = Currency::find()->select('short_name')->all();
        foreach ($model as $value) {
            $out['currency'][] = $value['short_name'];
        }

        $typeData = ProjectType::findOne($project_type_id);
        if (!$typeData) {
            $error = 'Price type data not found.';
        }

        $orderPrices = $prices;

        if ($error == '' && $typeData->format_is_custom && (int)$opts['words'] === 0) {
            $error = 'Words count not defined for custom project';
        }
        if ($error == '' && empty($orderPrices)) {
            $error = 'Unknown orders price list';
        }
        if ($error == '' && empty($orderPrices[$typeData->format_code])) {
            $error = 'Unknown prices for the format code: ' . $typeData->format_code;
        }
        if ($error == '' && empty($orderPrices[$typeData->format_code]['prices'][$typeData->price_type])) {
            $error = 'Unknown prices for the format code: ' . $typeData->format_code;
        }

        if ($error != '') {
            $out['error'] = $error;
        }
        if ($error == '') {

            if ($typeData->format_is_custom) {
                $words = (int)$opts['words'];
            } else {
                $words = (int)$typeData->words;
            }

            $wordsSelected = null;
            foreach ($orderPrices[$typeData->format_code]['prices'][$typeData->price_type] as $wordCount => $Value) {
                if ((int)$wordCount >= $words) {
                    $wordsSelected = (int)$wordCount;
                    break;
                }
            }

            if (!is_null($wordsSelected)) {
                $out['prices'] = $orderPrices[$typeData->format_code]['prices'][$typeData->price_type][$wordsSelected][$currency_id];
            }

            if (is_null($wordsSelected)) {
                $words = ceil($words / 100) * 100;
                $tmpPrices = $orderPrices['custom_prices']['prices'][$typeData->price_type][$currency_id];

                $out['prices'] = [
                    'price' => round(($tmpPrices['price'] * $words), 2),
                    'price_cw' => round(($tmpPrices['price_cw'] * $words), 2),
                    'editor' => round(($tmpPrices['editor'] * $words), 2),
                    'editor_revision' => round(($tmpPrices['editor_revision'] * $words), 2),
                ];
            }
            $out['words'] = $words;

            if ($opts['promocode_id'] !== '') {
                $model = Promocode::findOne($opts['promocode_id']);
                if ($model) {
                    $out['prices']['price_cw'] = $out['prices']['price_cw'] - (($out['prices']['price_cw'] / 100) * $model->client_discount);
                }
            }
        }
        return $out;
    }

    /**
     * @param int $code
     * @param string $text
     * @return string
     */
    public static function errorMessage($code = 0, $text = '')
    {
        if ($text !== '') {
            return $text;
        }
        if (isset(Yii::$app->params['exception'][$code])) {
            return Yii::$app->params['exception'][$code];
        }

        return 'Something went wrong';
    }

    /**
     * @param $to
     * @param $from
     * @param $subject
     * @param $body
     * @param array $options
     * @return bool
     */
    public static function sendEmail($to, $from, $subject, $body, $options = [])
    {
        $defaults = [
            'force' => false
        ];
        $opts = array_merge($defaults, $options);
        $a = null;

        $excludedDb = ['getacopywriter_vk', 'getacopywriter_vi', 'getacopywriter_test', 'getacopywriter_api', 'getacopywriter_az', 'getacopywriter_preprod'];

        $sendMail = false;
        $model = Config::findOne('send_mail');
        if ($model && (int)$model->value === 1) {
            $sendMail = true;
        }

        if ($sendMail && in_array(Yii::$app->params['dbname'], $excludedDb)) {
            $sendMail = false;
        }

        //Override sending mail for Production. Must always send
        if (!Yii::$app->params['is_stage']) {
            $sendMail = true;
        }

        if ($sendMail || (!$sendMail && $opts['force'])) {
            $subject = Yii::$app->params['emailSubjectPrefix'] . $subject;

            $a = Yii::$app->mailer->compose('default', ['body' => $body])
                ->setFrom($from)
                ->setTo($to)
                ->setSubject($subject)
                ->send();
        }


        $logTo = $logFrom = [];
        if (is_array($to)) {
            foreach ($to as $email => $name) {
                $logTo[] = $name . ' <' . $email . '>';
            }
        }
        else {
            $logTo[] = $to;
        }
        if (is_array($from)) {
            foreach ($from as $email => $name) {
                $logFrom[] = $name . ' <' . $email . '>';
            }
        }
        else {
            $logFrom[] = $from;
        }

        $result = 'NOK';
        if ($a) {
            $result = 'OK';
        }
        $logArr = [];
        $logArr[] = "\n\n" . date("Y-m-d H:i:s", strtotime(Yii::$app->params['now'])) . ' ' . $result;
        $logArr[] = "-------------------------------------------------------------";
        $logArr[] = 'From: ' . implode(', ', $logFrom);
        $logArr[] = 'To: ' . implode(', ', $logTo);
        $logArr[] = 'Subject: ' . $subject;
        Utils::log('api_email', implode("\n", $logArr));

        $logArr[] = $body;
        Utils::log('api_email_full', implode("\n", $logArr));

        if ($result === 'OK') {
            return true;
        }
        return false;
    }

    /**
     * @param $route
     * @param $action
     * @param string $message
     */
    public static function log($route, $action, $message = '')
    {
        $log = [];
        $log[] = $action;
        if ($message !== '') {
            $log[] = $message;
        }

        Yii::info(implode("\n", $log), $route);
    }

    /**
     * @param $message
     * @param array $data
     * @param string $category
     */
    public static function baseLog($message, $data = [], $category = '')
    {
        $log = [];
        $log[] = $message;
        if (!empty($data)) {
            $log = array_merge($log, $data);
        }
        Yii::info(implode("\n", $log), $category);
    }

    /**
     * @param $message
     * @param array $data
     */
    public static function logPayment($message, $data = [])
    {
        self::baseLog($message, $data, 'payments');
    }

    /**
     * @param $message
     * @param array $data
     */
    public static function logBraintree($message, $data = [])
    {
        self::baseLog($message, $data, 'braintree');
    }

    /**
     * @param $message
     * @param array $data
     */
    public static function logStripe($message, $data = [])
    {
        self::baseLog($message, $data, 'stripe');
    }

    /**
     * @param $message
     * @param array $data
     */
    public static function logPaypal($message, $data = [])
    {
        self::baseLog($message, $data, 'paypal');
    }

    /**
     * @param $message
     * @param array $data
     */
    public static function logApiKey($message, $data = [])
    {
        self::baseLog($message, $data, 'key');
    }

    /**
     * @param bool $debug
     * @return string
     */
    public static function f1($debug = false)
    {
        global $myNr;
        $myNr++;

        //Returns the calling function through a backtrace
        $r = '';
        $debugString = '';
        $caller = debug_backtrace();

        if ($debug === true) {
            $debugString .= ' <p>START: debug_backtrace result:</p>';
            $debugString .= '<pre>';
            $debugString .= print_r($caller, true);
            $debugString .= '</pre>';
            $debugString .= '<p>END: debug_backtrace result:</p>';
        }
        $caller = $caller[2];
        $r .= $caller['function'] . '()';
        if (isset($caller['class'])) {
            $r .= ' in ' . $caller['class'];
        }
        if (isset($caller['object'])) {
            $r .= ' (' . get_class($caller['object']) . ')';
        }

        echo "!!!!{$myNr} - {$r}{$debugString}!!!!\n";
        return $r . $debugString;
    }

    /**
     * @return mixed|\yii\db\Connection
     */
    public static function getDb()
    {
        $db = Yii::$app->getDb();

        if (Yii::$app->params['is_stage']) {
            $session = Yii::$app->session;

            $file = Yii::getAlias('@app/db.cnf');
            if (@file_exists($file)) {
                $tmp = @file($file);
                $db_postfix = $tmp[0];
                $db_string = 'db_' . $db_postfix;
                if (isset(Yii::$app->{$db_string})) $db = Yii::$app->{$db_string};
            }

            if (isset($session['ACTIVE_DB'])) {
                $db_postfix = str_replace('getacopywriter', '', $session['ACTIVE_DB']);
                $db_string = 'db' . $db_postfix;
                if (isset(Yii::$app->{$db_string})) $db = Yii::$app->{$db_string};
            }
        }
        return $db;
    }

    /**
     * @param array $options
     * @return array
     */
    public static function solvePrimaryId($options = [])
    {
        $defaults = [
            'country' => '',
            'email' => '',
            'case' => 'lead'
        ];
        $opts = array_merge($defaults, $options);
        $out = [
            'primaryId' => false,
            'history' => '',          //if the AM found from the existing clients and or leads
            'admins' => [],
            'debug' => []
        ];


        //Retrieve admins list, based on received country
        $filter = "a.active=1 AND FIND_IN_SET(\"" . strtoupper($opts['country']) . "\", UPPER(a.role_countries))>0 AND IFNULL(a.role_share,\"\")!=\"\"";
        $adminList = Admin::find()->from('admins a')->where($filter)->asArray()->all();

        if (empty($adminList)) {
            return $out;
        }

        $baseTotal = 0;
        $adminIDs = $admins = [];
        foreach ($adminList as $key => $value) {
            $baseTotal += (int)$value['role_share'];
            $adminIDs[] = $value['id'];
            $admins[$value['id']] = [
                'id' => $value['id'],
                'name' => $value['username'],
                'baseWeight' => $value['role_share'],        //admins.role_share
                'baseTotal' => 0,                           //Sum of admins.role_share per selected country
                'basePercent' => 0,                           //% of roles share for AM
                'itemWeight' => 0,                           //total items per selected AM
                'itemTotal' => 0,                           //total items per selected country (all related AM)
                'itemPercent' => 0,                           //% or items for AM
                'difference' => 0,                           //Difference between roleSharePercent - itemsPercent
                'lastUsed' => ''                           //key, when used AM for the last time. Used when difference for AM is the same
            ];
        }

        foreach ($admins as $id => $value) {
            $admins[$id]['baseTotal'] = $baseTotal;
            $admins[$id]['basePercent'] = round(($value['baseWeight'] / $baseTotal), 3);
        }

        if ($opts['case'] == 'lead') {

            //Search clients to match new lead email. If client primary contact found, return and exit;
            if ($opts['email'] != '' && !in_array(strtolower($opts['email']), ['-', 'see transcript', 'n/a@gmail.com', 'noemail@gmail.com'])) {

                $criteria = new Query();
                $criteria->select('com.am_id');
                $criteria->from('companies com');
                $criteria->where('IFNULL(com.am_id,\'\')!=\'\' AND IFNULL((SELECT users.id FROM users WHERE users.company_id=com.id AND users.email LIKE \'' . $opts['email'] . '\'),\'\')!=\'\'');
                $criteria->orderBy('com.create_date DESC');
                $criteria->limit(1);
                $results = $criteria->all();

                if (!empty($results)) {
                    $out['primaryId'] = (int)$results[0]->am_id;
                    $out['history'] = 'in_clients';
                    return $out;
                }

                //Search leads to match new lead email. If lead primary contact found, return and exit;
                $criteria = new Query();
                $criteria->select('l.primary_contact');
                $criteria->from('leads l');
                $criteria->where('IFNULL(l.primary_contact,\'\')!=\'\' AND l.email LIKE \'' . $opts['email'] . '\'');
                $criteria->orderBy('l.date_created DESC');
                $criteria->limit(1);
                $results = $criteria->all();
                if (!empty($results)) {
                    $out['primaryId'] = (int)$results[0]->primary_contact;
                    $out['history'] = 'in_leads';
                    return $out;
                }
            }

            $criteria = new Query();
            $criteria->select('l.chat_id AS id, l.primary_contact AS am_id');
            $criteria->from('leads l');
            $criteria->where('IFNULL(l.primary_contact,\'\') IN (' . implode(',', $adminIDs) . ')');
            $criteria->orderBy('l.date_created DESC, l.id DESC');
            $results = $criteria->all();
        }


        if ($opts['case'] == 'company') {
            $criteria = new Query();
            $criteria->select('id, am_id');
            $criteria->from('companies');
            $criteria->where('IFNULL(am_id,0) IN (' . implode(',', $adminIDs) . ')');
            $criteria->orderBy('create_date DESC');
            $results = $criteria->all();
        }
        $resultsTotal = count($results);

        foreach ($results as $key => $value) {
            $adminId = $value['am_id'];
            $admins[$adminId]['itemWeight']++;
            $admins[$adminId]['itemTotal'] = $resultsTotal;
            if ($admins[$adminId]['lastUsed'] == '') {
                $admins[$adminId]['lastUsed'] = 'key_' . $value['id'];
            }
        }

        foreach ($admins as $id => $value) {
            if ($admins[$id]['itemTotal'] != 0) {
                $admins[$id]['itemPercent'] = round(($value['itemWeight'] / $admins[$id]['itemTotal']), 3);
            }
            $admins[$id]['difference'] = round(($admins[$id]['basePercent'] - $admins[$id]['itemPercent']), 3);
        }

        //Retrieve AM who has biggest difference
        ArrayHelper::sortArrayByValue($admins, 'difference', 'DESC');
        reset($admins);
        $adminId = key($admins);
        $out['primaryId'] = $adminId;
        $out['admins'] = $admins;

        //Sort admins array by differences (greatest first)
        $tmp = ArrayHelper::searchArrayByValue($admins, 'difference', $admins[$adminId]['difference']);

        //If number of differences more than 1
        if (count($tmp) > 1) {
            $tmp1 = $tmp;

            //Sort sorted admins array by AM basePercent (greatest first)
            ArrayHelper::sortArrayByValue($tmp1, 'basePercent', 'DESC');

            //Assign first sorted element by default
            $adminId = $tmp[key($tmp1)]['id'];
            $out['primaryId'] = $adminId;

            //Get all AM with the same basePercent
            $tmp2 = ArrayHelper::searchArrayByValue($tmp1, 'basePercent', $admins[$adminId]['basePercent']);

            //If number of AM with the same basePercent is more than 2, assign AM by history (last used lead)
            if (count($tmp2) > 1) {
                ArrayHelper::sortArrayByValue($tmp, 'lastUsed', 'ASC');
                reset($tmp);
                $adminId = $tmp[key($tmp)]['id'];
                $out['primaryId'] = $adminId;
            }
        }

        return $out;
    }

    /**
     * @return float|int
     */
    public static function copywriterCut()
    {
        $model = Config::findOne(['param' => 'copywriter_cut']);
        if (!$model) {
            return 50;
        }
        return (float)$model->value;
    }

    /**
     * @param $field
     * @return string
     */
    public static function getConfig($field)
    {
        $model = Config::findOne(['param' => $field]);
        if (!$model) {
            return '';
        }
        return $model->value;
    }

    /**
     * @param int $length
     * @param string $prefix
     * @return string
     * @throws \Exception
     */
    public function unique($length = 13, $prefix = '')
    {

        // uniqid gives $length chars, but you could adjust it to your needs.
        if (function_exists("random_bytes")) {
            $bytes = random_bytes(ceil($length / 2));
        } elseif (function_exists("openssl_random_pseudo_bytes")) {
            $bytes = openssl_random_pseudo_bytes(ceil($length / 2));
        } else {
            throw new Exception("no cryptographically secure random function available");
        }

        return $prefix . substr(bin2hex($bytes), 0, $length);
    }

    /**
     * @param $string
     * @return bool
     */
    public function isJson($string)
    {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}
