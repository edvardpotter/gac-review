<?php

namespace app\components\utils;

/**
 * Class FileHelper
 * @package app\components\utils
 */
class FileHelper
{
    /**
     * @param $file
     * @param array $options
     * @return int
     */
    public static function getFilesize($file, $options = [])
    {
        if (!@file_exists($file)) {
            return 0;
        }

        return filesize($file);
    }
}