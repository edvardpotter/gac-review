<?php

namespace app\components\utils;

use app\api\modules\v1\models\Country;
use app\api\modules\v1\models\CountryPhone;
use app\components\Utils;
use Yii;


/**
 * Class IpHelper
 * @package app\components\utils
 */
class IpHelper
{
    /**
     * @return mixed
     * @throws \yii\db\Exception
     */
    public static function getPhoneByCountry()
    {
        $country = self::getCountryByIP2Location('', ['output' => 'all']);
        $codeId = 999;
        $country['country_code'] = strtoupper($country['country_code']);
        $out = ['phone' => '', 'country' => $country['country_code'], 'EU' => 0, 'countryPhoneId' => '', 'IP' => $country['ip']];
        if ($country['country_code'] === 'UK') {
            $country['country_code'] = 'GB';
        }
        $model = Country::findOne(['country_code' => $country['country_code']]);
        if ($model) {
            $codeId = $model->id;
        }

        $phoneModel = CountryPhone::findOne(['country_id' => $codeId]);
        if ($phoneModel) {
            $out['countryPhoneId'] = $codeId;
            $out['phone'] = $phoneModel->phone;
        } else {
            $phoneModel = CountryPhone::findOne(['country_id' => 999]);
            if ($phoneModel) {
                $out['countryPhoneId'] = $codeId;
                $out['phone'] = $phoneModel->phone;
            }
        }

        return $out['phone'];
    }

    /**
     * @param string $IP
     * @param array $options
     * @return array|mixed
     * @throws \yii\db\Exception
     */
    public static function getCountryByIP2Location($IP = '', $options = [])
    {
        $defaults = ['output' => 'country'];
        $opts = array_merge($defaults, $options);
        $out = ['country' => '', 'country_code' => '', 'ip' => '', 'ip_long' => ''];
        if ($IP == '') {
            $IP = Yii::$app->request->getUserIP();
        }
        $out['ip'] = $IP;

        $ip_version = strpos($IP, ":") === false ? 4 : 6;
        $ip_table = 'ip2location';

        if ($ip_version == 4) {
            $out['ip_long'] = self::ipv4toLong($IP);
            $ip_table = 'ip2location';
        }
        if ($ip_version == 6) {
            $out['ip_long'] = self::ipv6toLong($IP);
            $ip_table = 'ip2location_ipv6';
        }
        $db = Utils::getDb();

        $sql = "SELECT ip.*,cn.id AS country_id FROM " . $ip_table . " AS ip
            LEFT JOIN countries AS cn ON ip.country_code=cn.country_code
	        WHERE '" . $out['ip_long'] . "' BETWEEN ip_from AND ip_to LIMIT 1";
        $command = $db->createCommand($sql);

        if ($row = $command->queryOne()) {
            $out['country'] = $row['country_name'];
            $out['country_code'] = $row['country_code'];
            $out['country_id'] = $row['country_id'];
        }

        if ($opts['output'] != '' && $opts['output'] != 'all') {
            return $out[$opts['output']];
        }
        if ($opts['output'] == 'all') {
            return $out;
        }

        return $out;
    }

    /**
     * @param string $address
     * @return float|int
     */
    public static function ipv4toLong($address = '')
    {
        if ($address === '') {
            return 0;
        } else {
            $ips = explode('.', $address);
            return ($ips[3] + $ips[2] * 256 + $ips[1] * 256 * 256 + $ips[0] * 256 * 256 * 256);
        }
    }

    /**
     * @param $address
     * @return int|string
     */
    public static function ipv6toLong($address)
    {
        $int = inet_pton($address);
        $bits = 15;
        $ipv6long = 0;

        while ($bits >= 0) {
            $bin = sprintf("%08b", (ord($int[$bits])));

            if ($ipv6long) {
                $ipv6long = $bin . $ipv6long;
            } else {
                $ipv6long = $bin;
            }
            $bits--;
        }
        $ipv6long = gmp_strval(gmp_init($ipv6long, 2), 10);
        return $ipv6long;
    }

    /**
     * @return mixed
     * @throws \yii\db\Exception
     */
    public static function getPhoneByCountryVer2()
    {
        $country = self::getCountryByIP2Location('', ['output' => 'all']);
        $codeId = 999;
        $country['country_code'] = strtoupper($country['country_code']);
        $out = ['phone' => '', 'country' => $country['country_code'], 'EU' => 0, 'countryPhoneId' => '', 'IP' => $country['ip']];
        if ($country['country_code'] === 'UK') {
            $country['country_code'] = 'GB';
        }
        $model = Country::findOne(['country_code' => $country['country_code']]);
        if ($model) {
            $codeId = $model->id;
            if ((int)$model->eu === 1) {
                $codeId = 3;
                $out['EU'] = 1;
            }
            $phoneModel = CountryPhone::findOne(['country_id' => $codeId]);
            if (!$phoneModel) {
                $codeId = 999;
            }
        }
        $phoneModel = CountryPhone::findOne(['country_id' => $codeId]);
        $out['countryPhoneId'] = $codeId;
        $out['phone'] = $phoneModel->phone;
        return $phoneModel->phone;
    }


    /**
     * @return mixed
     * @throws \yii\db\Exception
     */
    public static function getPhoneByCountryVer1()
    {
        $out = [];
        $country = self::getCountryByIP2Location('', ['output' => 'all']);
        $phones = [
            'AU' => '1800 792 267',
            'CA' => '1.888.889.0150',
            'GB' => '0800 086 9066',
            'IR' => '1800 817 131',
            'EU' => '+44 330 808 0208',
            'OTHER' => '+1.888.889.0150'
        ];

        $country['country_code'] = strtoupper($country['country_code']);

        if ($country['country_code'] === 'UK') {
            $country['country_code'] = 'GB';
        }
        $model = Country::findOne(['country_code' => $country['country_code']]);
        if (!$model) {
            $code = 'OTHER';
        }
        else {
            $code = isset($phones[$country['country_code']]) ? $code = $country['country_code'] : 'OTHER';
            if ((int)$model->eu === 1) {
                $code = 'EU';
            }
        }
        return $phones[$code];
    }
}