<?php

namespace app\components\utils;

use app\api\modules\v1\models\Currency;

/**
 * Class CurrencyHelper
 * @package app\components\utils
 */
class CurrencyHelper
{
    /**
     * @param $amount
     * @param $fromCurrencyId
     * @param array $options
     * @return float
     */
    public static function convert($amount, $fromCurrencyId, $options = [])
    {
        $defaults = ['toCurrencyId' => 1];
        $opts = array_merge($defaults, $options);

        $currencyModelFrom = Currency::findOne($fromCurrencyId);
        if (!$currencyModelFrom) {
            return $amount;
        }

        $usdAmount = $outAmount = round(((float)$amount * (float)$currencyModelFrom->rate), 2);
        if ((int)$opts['toCurrencyId'] != 1) {
            $rate = 1;

            $currencyModelTo = Currency::findOne($opts['toCurrencyId']);
            if ($currencyModelTo) {
                $rate = (float)$currencyModelTo->rate;
            }

            $outAmount = round(($usdAmount / $rate), 2);
        }
        return $outAmount;
    }
}