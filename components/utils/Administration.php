<?php

namespace app\components\utils;

use app\api\modules\v1\models\ApiToken;
use app\components\Utils;
use Yii;

/**
 * Class Administration
 * @package app\components\utils
 */
class Administration
{
    /**
     * @param array $options
     * @return array
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    public static function importUsers($options = [])
    {
        $defaults = [
            'skipProtected' => false
        ];
        $opts = array_merge($defaults, $options);

        $total = ['am' => 0, 'user' => 0, 'copywriter' => 0, 'account' => 0];
        $out = [
            'total' => [],
            'success' => false
        ];

        $privateKeys = $publicKeys = $customKeys = [];

        if (Yii::$app->params['dbname'] === 'getacopywriter_stage') {
            $customKeys = [
                'user_117' => ['private_key' => 'private_key', 'protected' => 1, 'public_keys' => [0 => ['public_key' => 'public_key', 'callback_url' => 'http://com.com']]],
                'copywriter_4' => ['private_key' => 'VIT', 'protected' => 1, 'public_keys' => [0 => ['public_key' => 'vit', 'callback_url' => '']]],
                'am_6' => ['private_key' => 'VOFFKA', 'protected' => 1, 'public_keys' => [0 => ['public_key' => 'voffka', 'callback_url' => '']]],
            ];
            $privateKeys = ['private_key', 'VIT', 'VOFFKA'];
            $publicKeys = ['public_key', 'vit', 'voffka'];
        }

        $db = Utils::getDb();

        if (!$opts['skipProtected']) {
            $sql = "SELECT au.* FROM api_users au WHERE IFNULL(au.protected,0)=1 ORDER BY au.id";
            $rows = $db->createCommand($sql)->queryAll();
            foreach ($rows AS $key => $row) {
                $keyString = $row['user_type'] . '_' . $row['user_id'];
                if (!isset($customKeys[$keyString])) {
                    if (in_array($row['private_key'], $privateKeys)) {
                        continue;
                    }
                    $customKeys[$keyString] = ['private_key' => $row['private_key'], 'protected' => 1, 'public_keys' => []];
                    $privateKeys[] = $row['private_key'];
                }

                $sql1 = "SELECT at.* FROM api_tokens at WHERE at.api_user_id=" . (int)$row['id'] . " ORDER BY at.id";
                $rows1 = $db->createCommand($sql1)->queryAll();
                foreach ($rows1 AS $key1 => $row1) {
                    if (in_array($row1['public_key'], $publicKeys)) {
                        continue;
                    }
                    $customKeys[$keyString]['public_keys'][] = ['public_key' => $row1['public_key'], 'callback_url' => $row1['callback_url']];
                    $publicKeys[] = $row1['public_key'];
                }
            }
        }

        //Clearing up users and tokens
        $db->createCommand('TRUNCATE api_users')->execute();
        $db->createCommand('TRUNCATE api_tokens')->execute();

        //Clearing up permissions
        $auth = Yii::$app->authManager;
        $auth->removeAll();

        $guest = $auth->createRole('guest');
        $guest->description = 'Guest access';
        $auth->add($guest);

        $user = $auth->createRole('user');
        $user->description = 'User access';
        $auth->add($user);

        $writer = $auth->createRole('writer');
        $writer->description = 'Writer access';
        $auth->add($writer);

        $admin = $auth->createRole('am');
        $admin->description = 'Administrator access';
        $auth->add($admin);

        $auth->addChild($admin, $guest);
        $auth->addChild($admin, $user);
        $auth->addChild($admin, $writer);

        $out['roles'] = self::createRoles();


        //Import AM
        $rows = $db->createCommand('SELECT a.id,a.username FROM admins a ORDER BY a.id')->queryAll();
        foreach ($rows AS $key => $row) {
            $protected = 0;
            $privateKey = Utils::randomizer(['purpose' => 'apiPrivateKey', 'existing' => $privateKeys]);
            $publicKey = [0 => ['public_key' => Utils::randomizer(['purpose' => 'apiPublicKey', 'existing' => $publicKeys, 'callback' => function ($license) {
                return !is_null(ApiToken::find()->where('public_key = :public_key', [':public_key' => $license])->one());
            }]), 'callback_url' => '']];

            if (isset($customKeys['am_' . $row['id']])) {
                $protected = $customKeys['am_' . $row['id']]['protected'];
                $privateKey = $customKeys['am_' . $row['id']]['private_key'];
                $publicKey = $customKeys['am_' . $row['id']]['public_keys'];
            }

            $sql = "INSERT INTO api_users SET private_key='" . $privateKey . "',user_id=" . (int)$row['id'] . ",user_type='am',create_date=NOW(),protected=" . (int)$protected;
            $db->createCommand($sql)->execute();
            $apiUserId = $db->getLastInsertID();

            foreach ($publicKey as $keyNr => $pbKey) {
                $sql = "INSERT INTO api_tokens SET api_user_id=" . (int)$apiUserId . ",public_key='" . $pbKey['public_key'] . "',callback_url='" . $pbKey['callback_url'] . "',date_expire='2030-01-01 01:01:01'";
                $db->createCommand($sql)->execute();
            }

            $auth->assign($admin, $apiUserId);

            $total['am']++;
        }

        //Import users
        $rows = $db->createCommand('SELECT u.id, u.email FROM users u ORDER BY u.id')->queryAll();
        foreach ($rows AS $key => $row) {
            $protected = 0;
            $privateKey = Utils::randomizer(['purpose' => 'apiPrivateKey', 'existing' => $privateKeys]);
            $publicKey = [0 => ['public_key' => Utils::randomizer(['purpose' => 'apiPublicKey', 'existing' => $publicKeys, 'callback' => function ($license) {
                return !is_null(ApiToken::find()->where('public_key = :public_key', [':public_key' => $license])->one());
            }]), 'callback_url' => '']];

            if (isset($customKeys['user_' . $row['id']])) {
                $protected = $customKeys['user_' . $row['id']]['protected'];
                $privateKey = $customKeys['user_' . $row['id']]['private_key'];
                $publicKey = $customKeys['user_' . $row['id']]['public_keys'];
            }

            $sql = "INSERT INTO api_users SET private_key='" . $privateKey . "',user_id=" . (int)$row['id'] . ",user_type='user',create_date=NOW(),protected=" . (int)$protected;
            $db->createCommand($sql)->execute();
            $apiUserId = $db->getLastInsertID();

            foreach ($publicKey as $keyNr => $pbKey) {
                $sql = "INSERT INTO api_tokens SET api_user_id=" . (int)$apiUserId . ",public_key='" . $pbKey['public_key'] . "',callback_url='" . $pbKey['callback_url'] . "',date_expire='2030-01-01 01:01:01'";
                $db->createCommand($sql)->execute();
            }

            $auth->assign($user, $apiUserId);
            $total['user']++;
        }

        //Import copywriters
        $rows = $db->createCommand('SELECT cw.id, cw.email FROM copywriters cw ORDER BY cw.id')->queryAll();
        foreach ($rows AS $key => $row) {
            $protected = 0;
            $privateKey = Utils::randomizer(['purpose' => 'apiPrivateKey', 'existing' => $privateKeys]);
            $publicKey = [0 => ['public_key' => Utils::randomizer(['purpose' => 'apiPublicKey', 'existing' => $publicKeys, 'callback' => function ($license) {
                return !is_null(ApiToken::find()->where('public_key = :public_key', [':public_key' => $license])->one());
            }]), 'callback_url' => '']];

            if (isset($customKeys['copywriter_' . $row['id']])) {
                $protected = $customKeys['copywriter_' . $row['id']]['protected'];
                $privateKey = $customKeys['copywriter_' . $row['id']]['private_key'];
                $publicKey = $customKeys['copywriter_' . $row['id']]['public_keys'];
            }

            $sql = "INSERT INTO api_users SET private_key='" . $privateKey . "',user_id=" . (int)$row['id'] . ",user_type='writer',create_date=NOW(),protected=" . (int)$protected;
            $db->createCommand($sql)->execute();
            $apiUserId = $db->getLastInsertID();

            foreach ($publicKey as $keyNr => $pbKey) {
                $sql = "INSERT INTO api_tokens SET api_user_id=" . (int)$apiUserId . ",public_key='" . $pbKey['public_key'] . "',callback_url='" . $pbKey['callback_url'] . "',date_expire='2030-01-01 01:01:01'";
                $db->createCommand($sql)->execute();
            }

            $auth->assign($writer, $apiUserId);
            $total['copywriter']++;
        }
        $out['success'] = true;

        $out['total'] = ['am' => $total['am'], 'users' => $total['user'], 'writers' => $total['copywriter']];
        return $out;
    }


    /**
     * @param array $options
     * @return array
     * @throws \yii\base\Exception
     */
    public static function createRoles($options = [])
    {
        $defaults = [
            'updateRules' => ['IsOwner', 'checkUserRole'],
            'updatePermissions' => ['IsOwner', 'actAs', 'checkUserRole']
        ];
        $opts = array_merge($defaults, $options);

        $userRole = null;
        $auth = Yii::$app->authManager;

        //Get all user roles defined. Remove roles, starting with "role" prefix
        $roles = $auth->getRoles();

        if ($roles) {
            foreach ($roles as $key => $role) {
                if (substr($role->name, 0, 4) == 'role') {
                    $auth->remove($role);
                }
                if ($role->name === 'user') {
                    $userRole = $role;
                }
            }
        }

        //Get all user rules. Remove those, defined within updateRules
        $rules = $auth->getRules();
        if ($rules) {
            foreach ($rules as $key => $rule) {
                if (in_array($rule->name, $opts['updateRules'])) {
                    $auth->remove($rule);
                }
            }
        }

        //Get all permissions. Remove those, defined within updatePermissions
        $permissions = $auth->getPermissions();
        if ($permissions) {
            foreach ($permissions as $key => $permission) {
                if (in_array($permission->name, $opts['updatePermissions'])) {
                    $auth->remove($permission);
                }
            }
        }

        $rule = new \app\common\rbac\IsOwner;
        $auth->add($rule);
        $isOwner = $auth->createPermission('IsOwner');
        $isOwner->description = 'Validate user project ownership';
        $isOwner->ruleName = $rule->name;
        $auth->add($isOwner);


        $roleRule = new \app\common\rbac\CheckUserRole;
        $auth->add($roleRule);
        $roleRulePermission = $auth->createPermission('actAs');
        $roleRulePermission->description = 'Validate user role';
        $roleRulePermission->ruleName = $roleRule->name;
        $auth->add($roleRulePermission);

        if (!$userRole) {
            $userRole = $auth->createRole('user');
        }
        $auth->addChild($userRole, $isOwner);
        $auth->addChild($userRole, $roleRulePermission);


        return ['rules' => $auth->getRules(), 'permissions' => $auth->getPermissions()];
    }
}