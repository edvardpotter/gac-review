<?php

namespace app\components\utils;

use app\api\modules\v1\models\User;
use Yii;

/**
 * Class TimeHelper
 * @package app\components\utils
 */
class TimeHelper
{
    /**
     * @param $seconds
     * @param array $options
     * @return string
     */
    public static function formatSeconds($seconds, $options = [])
    {
        $defaults = array(
            'format' => 'nonzero',
            'show_sign' => true,
            'color_negative' => false,
            'delimiter' => ','
        );
        $opts = array_merge($defaults, $options);
        $Data = array(
            'sign' => ($seconds < 0) ? '-' : '',
            'days' => '',
            'hours' => '',
            'minutes' => '',
        );
        $out = $seconds;
        $secondsOriginal = $seconds;
        $seconds = abs($seconds);
        $Data['years'] = intval($seconds / (86400 * 365));
        $Data['months'] = intval($seconds / (86400 * 30));
        $Data['days'] = intval($seconds / 86400);
        $Data['hours'] = intval(($seconds - $Data['days'] * 86400) / 3600);
        $Data['minutes'] = intval(($seconds - $Data['days'] * 86400 - $Data['hours'] * 3600) / 60);

        if ($opts['format'] == 'nonzero') {
            $out = $Data['days'] . ' day' . (($Data['days'] == 1) ? '' : 's') . ' ' . $Data['hours'] . ' hour' . ((in_array($Data['hours'], array(1, 21))) ? '' : 's');
            if ($Data['hours'] == 0) {
                $out = $Data['days'] . ' day' . (($Data['days'] == 1) ? '' : 's');
                if ($Data['days'] == 7) {
                    $out = '1 week';
                }
                if ($Data['days'] == 30) {
                    $out = '1 month';
                }
            }
            if ($Data['days'] == 0) {
                $out = $Data['hours'] . ' hour' . ((in_array($Data['hours'], array(1, 21))) ? '' : 's');
            }
        }

        if ($opts['format'] == 'full') {
            $out = $Data['days'] . ' day' . (($Data['days'] == 1) ? '' : 's') . ' ' . $Data['hours'] . ' hour' . ((in_array($Data['hours'], array(1, 21))) ? '' : 's');
        }

        if ($opts['format'] == 'days') {
            $out = $Data['days'] * 24 + $Data['hours'];
        }

        if ($opts['format'] == 'details') {
            $out = $Data;
        }

        if ($opts['format'] == 'days_with_hours') {
            $out = $Data['days'] . ' day' . (((int)$Data['days'] != 1) ? 's' : '') . (($Data['hours'] != 0) ? ' ' . $Data['hours'] . ' hour' . (((int)$Data['hours'] != 1) ? 's' : '') : '');
        }

        if ($opts['format'] == 'd') {
            $out = $Data['days'] . 'd';
        }

        if ($opts['format'] == 'h') {
            $out = $Data['days'] * 24 + $Data['hours'] . 'h';
        }

        if ($opts['format'] == 'd_or_h') {
            if ($Data['days'] > 0) {
                $out = $Data['days'] . 'd';
            }
            if ($Data['days'] == 0) {
                $out = $Data['hours'] . 'h';
            }
        }

        if ($opts['format'] == 'dh_or_h') {
            if ($Data['days'] > 0) {
                $out = $Data['days'] . 'd ' . $Data['hours'] . 'h';
            }
            if ($Data['days'] == 0) {
                $out = $Data['hours'] . 'h';
            }
        }

        if ($opts['format'] == 'dh_or_hm') {
            if ($Data['days'] > 0) {
                $out = $Data['days'] . 'd ' . $Data['hours'] . 'h';
            }
            if ($Data['days'] == 0) {
                $out = $Data['hours'] . 'h ' . $Data['minutes'] . 'm';
            }
        }

        if ($opts['format'] == 'd_h') {
            $out = $Data['days'] . 'd ' . $Data['hours'] . 'h';
        }

        if ($opts['format'] == 'd_h_m') {
            $out = $Data['days'] . 'd' . $opts['delimiter'] . ' ' . $Data['hours'] . 'h' . $opts['delimiter'] . ' ' . $Data['minutes'] . 'm';
        }

        if ($opts['format'] == 'dh_or_hm_nozero') {
            if ($Data['days'] > 0) {
                $out = $Data['days'] . 'd' . (($Data['hours'] != 0) ? ' ' . $Data['hours'] . 'h' : '');
            }
            if ($Data['days'] == 0) {
                $out = $Data['hours'] . 'h' . (($Data['minutes'] != 0) ? ' ' . $Data['minutes'] . 'm' : '');
            }
        }

        if ($opts['format'] == 'days_past_future') {
            if ($Data['days'] > 0 && $Data['sign'] == '') {
                $out = 'In ' . $Data['days'] . 'd';
            }
            if ($Data['days'] > 0 && $Data['sign'] == '-') {
                $out = $Data['days'] . 'd ago';
            }
            if ($Data['days'] == 0) {
                $out = 'Now';
            }
        }

        if ($opts['format'] == 'minutes_to_years') {
            if ($Data['hours'] == 0 && $Data['days'] == 0 && $Data['months'] == 0 && $Data['years'] == 0) {
                $out = $Data['minutes'] . ' minute' . (($Data['minutes'] != 1) ? 's' : '') . ' ago';
            }
            if ($Data['hours'] != 0 && $Data['days'] == 0 && $Data['months'] == 0 && $Data['years'] == 0) {
                $out = $Data['hours'] . ' hour' . (($Data['hours'] != 1) ? 's' : '') . ' ago';
            }
            if ($Data['days'] != 0 && $Data['months'] == 0 && $Data['years'] == 0) {
                $out = $Data['days'] . ' day' . (($Data['days'] != 1) ? 's' : '') . ' ago';
            }
            if ($Data['months'] != 0 && $Data['years'] == 0) {
                $out = $Data['months'] . ' month' . (($Data['months'] != 1) ? 's' : '') . ' ago';
            }
            if ($Data['years'] != 0) {
                $out = $Data['years'] . ' year' . (($Data['years'] != 1) ? 's' : '') . ' ago';
            }
        }

        if ($opts['format'] == 'minutes_to_years_short') {
            $out = 'Now';
            if ($Data['minutes'] > 0) {
                $out = $Data['minutes'] . 'm ago';
            }
            if ($Data['hours'] > 0) {
                $out = $Data['hours'] . 'h ago';
            }
            if ($Data['days'] > 0) {
                $out = $Data['days'] . 'd ago';
            }
            if ($Data['months'] > 0) {
                $out = $Data['months'] . 'm ago';
            }
            if ($Data['years'] > 0) {
                $out = $Data['years'] . 'y ago';
            }
        }

        if ($opts['format'] == 'month_ago' && $Data['months'] > 0) {
            $out = $Data['months'] . 'm ago';
        }

        if ($opts['format'] == 'touch') {
            if ($secondsOriginal < 0) {
                $out = $Data['days'] . 'd ago';
                if ($Data['months'] > 0) {
                    $out = $Data['months'] . 'm ago';
                }
            }
            if ($secondsOriginal > 0) {
                $out = 'In ' . $Data['days'] . 'd';
                if ($Data['months'] > 0) {
                    $out = 'In ' . $Data['months'] . 'm';
                }
            }
            if ($Data['days'] == 0) {
                $out = 'Today';
            }
            if ((int)$secondsOriginal == 0) {
                $out = 'None';
            }
        }

        if ($opts['format'] == 'ms_or_s') { //minutes or minutes & seconds
            if ($Data['minutes'] == 0) {
                $out = $seconds . 's';
            }
            if ($Data['minutes'] > 0) {
                $out = $Data['minutes'] . 'm';
                if (($seconds - $Data['minutes'] * 60) > 0) {
                    $out .= ($seconds - $Data['minutes'] * 60) . 's';
                }
            }
        }

        if ($opts['format'] == 'hm_or_m') { //hours or hours & minutes
            if ($Data['hours'] == 0) {
                $out = $Data['minutes'] . 'm';
            }
            if ($Data['hours'] > 0) {
                $out = $Data['hours'] . 'h';
                if ($Data['minutes'] > 0) {
                    $out .= ' ' . $Data['minutes'] . 'm';
                }
            }
        }

        if ($opts['format'] == 'mhdmy') { //Years, months, days, hours or minutes ago
            $out = 'Now';
            if ($Data['minutes'] > 0) {
                $out = $Data['minutes'] . ' mins ago';
            }
            if ($Data['hours'] > 0) {
                $out = $Data['hours'] . ' hours ago';
            }
            if ($Data['days'] > 0) {
                $out = $Data['days'] . ' days ago';
            }
            if ($Data['months'] > 0) {
                $out = $Data['months'] . ' months ago';
            }
            if ($Data['years'] > 0) {
                $out = $Data['years'] . ' years ago';
            }
        }

        if ($opts['format'] == 'dhm_or_hm_or_m') {
            if ($Data['days'] > 0) {
                $out = $Data['days'] . 'd ' . $Data['hours'] . 'h ' . $Data['minutes'] . 'm';
            }
            if ($Data['days'] == 0) {
                $out = $Data['minutes'] . 'm';
                if ($Data['hours'] > 0) {
                    $out = $Data['hours'] . 'h ' . $out;
                }
            }
            $out .= ' ago';
        }

        if ($opts['show_sign']) {
            $out = $Data['sign'] . $out;
        }

        return $out;
    }


    /**
     * @param $date
     * @param array $options
     * @return false|string
     */
    public static function convertUTC($date, $options = [])
    {
        $defaults = [
            'direction' => 'toUTC',    //converts to UTC
            'user_id' => '',
            'timezone' => ''
        ];
        $opts = array_merge($defaults, $options);
        $out = ['sign' => 1, 'hours' => 0, 'minutes' => 0, 'totalMinutes' => 0, 'dateUTC' => $date];

        if ($opts['timezone'] === 'loggedUser') {
            $opts['timezone'] = Yii::$app->params['user']['timezone'];
        }
        if ((int)$opts['user_id'] === 0 && $opts['timezone'] === '') {
            return $date;
        }
        if ((string)$date === '') {
            return $date;
        }

        if ($opts['timezone'] === '') {
            $user = User::findOne((int)$opts['user_id']);
            if (!$user || ($user && (string)$user->timezone === '')) {
                return $date;
            }
            $opts['timezone'] = $user->timezone;
        }

        $tmp = explode(':', $opts['timezone']);
        if (strpos($tmp[0], '-') === false) {
            $out['sign'] = -1;
        }
        if ($opts['direction'] === 'fromUTC') {
            $out['sign'] = $out['sign'] * -1;
        }

        if (isset($tmp[0])) {
            $out['hours'] = abs((int)$tmp[0]);
        }
        if (isset($tmp[1])) {
            $out['minutes'] = (int)$tmp[1];
        }

        $out['totalMinutes'] = $out['hours'] * 60 + $out['minutes'];
        $out['dateUTC'] = date("Y-m-d H:i:s", strtotime(($out['totalMinutes'] * $out['sign']) . ' minutes', strtotime($date)));
        return $out['dateUTC'];
    }


    /**
     * @param $start
     * @param $end
     * @param array $options
     * @return float|string
     */
    public static function dateDiff($start, $end, $options = [])
    {
        $defaults = ['format' => 'hours'];
        $opts = array_merge($defaults, $options);

        $startTimestamp = strtotime($start);
        $endTimestamp = strtotime($end);
        $seconds = $endTimestamp - $startTimestamp;
        $hours = $seconds / 3600;
        $out = '';

        if ($opts['format'] === 'hours') {
            $out = floor($hours);
        }

        return $out;
    }

}
