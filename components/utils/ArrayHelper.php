<?php

namespace app\components\utils;


/**
 * Class ArrayHelper
 * @package app\components\utils
 */
class ArrayHelper
{
    /**
     * @param $array
     * @param $search_key
     * @param $insert_key
     * @param $insert_value
     * @param bool $insert_after_founded_key
     * @param bool $append_if_not_found
     * @return array
     */
    public static function insert_into_array($array, $search_key, $insert_key, $insert_value, $insert_after_founded_key = true, $append_if_not_found = false)
    {
        $new_array = [];

        foreach ($array as $key => $value) {
            // INSERT BEFORE THE CURRENT KEY?
            // ONLY IF CURRENT KEY IS THE KEY WE ARE SEARCHING FOR, AND WE WANT TO INSERT BEFORE THAT FOUNDED KEY
            if ($key === $search_key && !$insert_after_founded_key) {
                $new_array[$insert_key] = $insert_value;
            }

            // COPY THE CURRENT KEY/VALUE FROM OLD ARRAY TO A NEW ARRAY
            $new_array[$key] = $value;

            // INSERT AFTER THE CURRENT KEY?
            // ONLY IF CURRENT KEY IS THE KEY WE ARE SEARCHING FOR, AND WE WANT TO INSERT AFTER THAT FOUNDED KEY
            if ($key === $search_key && $insert_after_founded_key) {
                $new_array[$insert_key] = $insert_value;
            }
        }

        // APPEND IF KEY ISNT FOUNDED
        if ($append_if_not_found && count($array) == count($new_array)) {
            $new_array[$insert_key] = $insert_value;
        }

        return $new_array;
    }


    /**
     * @param $array
     * @param $key
     * @param string $direction
     */
    public static function sortArrayByValue(&$array, $key, $direction = 'ASC')
    {
        $sorter = [];
        $ret = [];
        reset($array);
        foreach ($array as $ii => $va) {
            $sorter[$ii] = $va[$key];
        }

        if (strtolower($direction) == 'asc') {
            asort($sorter);
        }
        if (strtolower($direction) == 'desc') {
            arsort($sorter);
        }

        foreach ($sorter as $ii => $va) {
            $ret[$ii] = $array[$ii];
        }

        $array = $ret;
    }

    /**
     * @param $array
     * @param $key
     * @param $value
     * @return array
     */
    public static function searchArrayByValue($array, $key, $value)
    {
        $results = array();

        if (is_array($array)) {
            if (isset($array[$key]) && $array[$key] == $value) {
                $results[] = $array;
            }

            foreach ($array as $subarray) {
                $results = array_merge($results, self::searchArrayByValue($subarray, $key, $value));
            }
        }

        return $results;
    }


    /**
     * @param $sortArray
     * @param $orderArray
     * @param $fieldOnSort
     * @return mixed
     */
    public static function sortArrayAgainstList($sortArray, $orderArray, $fieldOnSort)
    {
        $arrayTotal = count($sortArray);

        //Example from views/client/projects_list.php

        //Retrieving passed array keys
        $keys = array_flip($orderArray);
        //Sorting out passed array against selected field ($fieldOnSort)
        usort($sortArray, function ($a, $b) use ($keys, $fieldOnSort, $arrayTotal) {
            //If the element is not defined within orderArray pass it to the end of resulting array
            if (!isset($keys[$a[$fieldOnSort]]) || !isset($keys[$b[$fieldOnSort]])) {
                return $arrayTotal;
            }
            return $keys[$a[$fieldOnSort]] - $keys[$b[$fieldOnSort]];
        });

        return $sortArray;
    }


    /**
     * @param $obj
     * @return array
     */
    public static function convertObjectToArray($obj)
    {
        if (is_object($obj)) {
            $obj = (array)$obj;
        }
        if (is_array($obj)) {
            $new = [];
            foreach ($obj as $key => $val) {
                $new[$key] = self::convertObjectToArray($val);
            }
        } else {
            $new = $obj;
        }

        return $new;
    }

    /**
     * @param $arr
     * @param array $options
     * @return string
     */
    public static function implodeArray($arr, $options = [])
    {
        $defaults = [
            'separator' => "\n"
        ];
        $opts = array_merge($defaults, $options);

        return implode($opts['separator'], array_map(
            function ($v, $k) {
                $value = $v;
                if (is_array($v)) {
                    if (isset($v[0])) {
                        $value = $v[0];
                    } else {
                        $value = '';
                    }
                }

                if (is_array($value)) {
                    $value = serialize($value);
                }
                return str_pad($k, 20, ' ', STR_PAD_RIGHT) . ': ' . $value;
            },
            $arr,
            array_keys($arr)
        ));
    }
}