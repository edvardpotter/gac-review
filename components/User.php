<?php

namespace app\components;

use Yii;

/**
 * Extended yii\web\User
 *
 * This allows us to do "Yii::$app->user->something" by adding getters
 * like "public function getSomething()"
 *
 * So we can use variables and functions directly in `Yii::$app->user`
 */
class User extends \yii\web\User
{

    /**
     * @return int
     */
    public function getUserId()
    {
        return (int)Yii::$app->user->identity->user_id;
    }

    /**
     * @return mixed
     */
    public function getUserType()
    {
        return Yii::$app->user->identity->user_type;
    }
}