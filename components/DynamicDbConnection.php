<?php

namespace app\components;

use Yii;
use yii\base\Component;


/**
 * Class DynamicDbConnection
 * @package app\components
 */
class DynamicDbConnection extends Component
{
    /**
     * @var string
     */
    private $configFile = 'db.cnf';

    /**
     * @throws \yii\db\Exception
     */
    function init()
    {
        parent::init();
        $error = '';

        if (Yii::$app->params['is_stage']) {
            $file = realpath(Yii::$app->basePath) . '/' . $this->configFile;

            if ($error === '' && !@file_exists($file)) {
                $error = 'No config file found';
            }
            if ($error == '') {
                $tmp = @file($file);
                $db_postfix = $tmp[0];
                $db_string = 'db_' . $db_postfix;
                $db_file = Yii::getAlias('@config/' . $db_string . '.php');

                if (@file_exists($db_file)) {
                    $db_params = require $db_file;
                    if (isset($db_params['dsn'])) {
                        Yii::$app->db->close();
                        Yii::$app->db->dsn = $db_params['dsn'];
                        Yii::$app->db->open();
                    }
                }
            }
        }
    }
}