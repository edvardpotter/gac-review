<?php

$db_stage = require(__DIR__ . '/db_stage.php');
$params = require(__DIR__ . '/params_stage.php');

$config = [
    'id' => 'GAC',
    'name' => 'API GetACopywriter',
    // Need to get one level up:
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'session' => [
            'savePath' => '/var/www-sessions/sessions_dev/',

            'name' => 'GACSESSID',
            'cookieParams' => [
                'domain' => '.getacopywriter.com',
                'httpOnly' => true,
            ],
        ],
        'user' => [
            'class' => 'app\components\User',
            'identityClass' => 'app\api\modules\v1\models\ApiUser',
            'enableAutoLogin' => false,
            'enableSession' => true,

            'identityCookie' => [
                'name' => 'GACSESSID',
                'httpOnly' => true,
                'domain' => '.getacopywriter.com',
            ],
        ],
        'authManager' => [
            'class' => 'app\common\rbac\MyDbManager',   //store RBAC in database (custom db detection)
        ],
        'request' => [
            // Enable JSON Input:
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
            'enableCookieValidation' => false,
        ],
        'log' => [
            'traceLevel' => 3,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning', 'info'],
                    'logFile' => '@app/runtime/logs/api.log',
                    'logVars' => ['_FILES', '_SESSION'],
                    'except' => [
                        'yii\web\HttpException:400',
                        'api_vars',
                    ],
                    'maxFileSize' => 20480,
                    'maxLogFiles' => 20,
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@app/runtime/logs/error.log',
                    'categories' => ['api_error'],
                    'logVars' => [],
                    'except' => ['yii\*'],
                    'maxFileSize' => 20480,
                    'maxLogFiles' => 20,
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@app/runtime/logs/email.log',
                    'categories' => ['api_email'],
                    'logVars' => [],
                    'maxFileSize' => 20480,
                    'maxLogFiles' => 20,
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@app/runtime/logs/email_full.log',
                    'categories' => ['api_email_full'],
                    'logVars' => [],
                    'maxFileSize' => 20480,
                    'maxLogFiles' => 20,
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@app/runtime/logs/client.log',
                    'categories' => ['api_client'],
                    'logVars' => [],
                    'maxFileSize' => 20480,
                    'maxLogFiles' => 20,
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@app/runtime/logs/payments.log',
                    'categories' => ['payments'],
                    'logVars' => ['*'],
                    'maxFileSize' => 20480,
                    'maxLogFiles' => 20,
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@app/runtime/logs/sql.log',
                    'categories' => ['yii\db\Command::query', 'yii\db\Command::execute'],
                    'logVars' => [],
                    'maxFileSize' => 20480,
                    'maxLogFiles' => 20,
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@app/runtime/logs/api_generated_key.log',
                    'categories' => ['key'],
                    'logVars' => ['*'],
                    'maxFileSize' => 20480,
                    'maxLogFiles' => 20,
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@app/runtime/logs/api_vars.log',
                    'categories' => ['api_vars'],
                    'logVars' => ['*'],
                    'maxFileSize' => 20480,
                    'maxLogFiles' => 20,
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [

                'GET,OPTIONS <pubkey:\w+>/v1/users' => 'v1/users/index',
                'GET,OPTIONS <pubkey:\w+>/v1/user_details' => 'v1/users/details',
                'GET,OPTIONS <pubkey:\w+>/v1/users/<user_uid:\w+>/keys' => 'v1/users/keys',
                'POST,OPTIONS <pubkey:\w+>/v1/users' => 'v1/users/create',
                'DELETE,OPTIONS <pubkey:\w+>/v1/users/<unique_id:\w+>' => 'v1/users/delete',
                'PUT,OPTIONS <pubkey:\w+>/v1/users/<user_uid:\w+>/update' => 'v1/users/update',
                'PUT,OPTIONS <pubkey:\w+>/v1/users/<user_uid:\w+>/<account_unique_id:\w+>/add' => 'v1/users/add-account',
                'DELETE,OPTIONS <pubkey:\w+>/v1/users/<unique_id:\w+>/<account_unique_id:\w+>' => 'v1/users/delete-account',
                'GET,OPTIONS /v1/users/email_exists' => 'v1/users/email-exists',
                'GET,OPTIONS /v1/restore_password' => 'v1/users/restore-password',
                'PUT,OPTIONS /v1/set_password' => 'v1/users/set-password',
                'POST,OPTIONS /v1/prospect' => 'v1/users/add-prospect',
                //Assets
                'GET,OPTIONS <fakePubkey:\w+>/v1/formats' => 'v1/asset/formats',
                'GET,OPTIONS <fakePubkey:\w+>/v1/projects/statuses' => 'v1/asset/project-statuses',

                'GET,OPTIONS /v1/suggested_word_count' => 'v1/asset/suggested-word-count',
                'GET,OPTIONS /v1/content_budgets' => 'v1/asset/content-budgets',
                'GET,OPTIONS /v1/countries' => 'v1/asset/countries',
                'GET,OPTIONS /v1/currencies' => 'v1/asset/currencies',
                'GET,OPTIONS /v1/turnaround' => 'v1/asset/turnaround',
                'GET,OPTIONS /v1/bundles' => 'v1/asset/bundles',
                'GET,OPTIONS /v1/rates' => 'v1/asset/rates',
                'GET,OPTIONS /v1/user_roles' => 'v1/asset/user-roles',
                'GET,OPTIONS /v1/industries' => 'v1/asset/industries',
                'GET,OPTIONS /v1/formats' => 'v1/asset/formats',
                'GET,OPTIONS /v1/projects/statuses' => 'v1/asset/project-statuses',
                'GET,OPTIONS /v1/copystyles' => 'v1/asset/copystyles',
                'GET,OPTIONS /v1/copyvoices' => 'v1/asset/copyvoices',
                'GET,OPTIONS /v1/contenttypes' => 'v1/asset/contenttypes',
                'GET,OPTIONS /v1/targetaudiences' => 'v1/asset/targetaudiences',
                'GET,OPTIONS /v1/localizations' => 'v1/asset/localizations',
                'GET,OPTIONS /v1/formattings' => 'v1/asset/formattings',

            ],
        ],
        'response' => [
            'format' => \yii\web\Response::FORMAT_JSON,
            'class' => \yii\web\Response::className(),

            'on beforeSend' => function ($event) {
                app\components\Utils::logRequests();
                $response = $event->sender;

                $responseData = [
                    'success' => 1,
                    'total' => 0,
                    'extra' => [],
                    'time' => sprintf('%0.3f seconds', Yii::$app->params['executionTime']['total']),
                    'time_yii' => sprintf('%0.3f seconds', Yii::getLogger()->getElapsedTime()),
                    'user' => Yii::$app->params['user'],
                    'database' => Yii::$app->params['dbname'],
                    'database_ar' => Yii::$app->params['dbname_ar'],
                    'platform' => 'api-dev',
                    'user' => [
                        'method' => Yii::$app->request->getMethod(),
                        'anonymousId' => isset(Yii::$app->params['user']['anonymousId']) ? Yii::$app->params['user']['anonymousId'] : 'undefined',
                        'ip' => Yii::$app->request->getUserIP(),
                        'id' => isset(Yii::$app->params['user']['id']) ? Yii::$app->params['user']['id'] : 'undefined',
                        'type' => isset(Yii::$app->params['user']['type']) ? Yii::$app->params['user']['type'] : 'undefined',
                        'timezone' => isset(Yii::$app->params['user']['timezone']) ? Yii::$app->params['user']['timezone'] : 'undefined',
                        'accountId' => isset(Yii::$app->params['user']['accountId']) ? Yii::$app->params['user']['accountId'] : 'undefined',
                    ],
                    'error' => [
                        'name' => '',
                        'status' => '',
                        'message' => '',
                    ],
                    'response' => ['']
                ];
                if (!Yii::$app->params['is_stage']) {
                    unset($responseData['time']);
                    unset($responseData['time_yii']);
                    unset($responseData['database']);
                    unset($responseData['database_ar']);
                    unset($responseData['user']);
                }

                $message = $status = '';
                if (isset($response->data['message'])) {
                    $message = $response->data['message'];
                }
                if (isset($response->data['status'])) {
                    $status = $response->data['status'];
                }


                //Override message and status, if exception handler passed with json string in the error field
                if (isset($response->data['message'])) {
                    $statusOverride = json_decode($response->data['message'], true);
                }
                if (isset($statusOverride) && $statusOverride !== null) {
                    $message = $statusOverride['error'];
                    $status = $statusOverride['status'];
                }

                if ($response->data !== null && ($exception = Yii::$app->getErrorHandler()->exception) !== null) {
                    $responseData['success'] = 0;
                    if (isset($response->data['name'])) {
                        $responseData['error']['name'] = $response->data['name'];
                    }
                    if (isset($response->data['status'])) {
                        $responseData['error']['status'] = $status;
                    }
                    if (isset($response->data['message'])) {
                        $responseData['error']['message'] = $message;
                    }

                    $response->data = $responseData;
                    Yii::info($message . ' [pubkey: "' . Yii::$app->request->get('pubkey') . '" user id: "' . Yii::$app->params['user']['id'] . '", type: "' . Yii::$app->params['user']['type'] . '")', 'api_error');

                    $email = new app\api\modules\v1\models\EmailMessage();
                    $email->sendOnError($status, ['message' => $message]);
                } else {
                    //override response->data in special cases
                    if (is_array($response->data) && isset($response->data['special'])) {

                        if (isset($response->data['response'])) {
                            $responseData['response'] = $responseData['response'] = $response->data['response'];
                        }
                        if (!empty($response->data['additionalFields'])) {
                            $responseData['extra'] = $response->data['additionalFields'];
                        }
                        if (is_array($responseData['response'])) {
                            $responseData['total'] = count($responseData['response']);
                        }
                        $response->data = $responseData;
                    } else {
                        $responseData['response'] = $response->data;
                        if ($response->data && is_array($response->data)) {
                            $responseData['total'] = count($response->data);
                        }

                        //check for $response->data (needed for project download endpoint) otherwise - error
                        if ($response->data || is_array($response->data)) {
                            $response->data = $responseData;
                        }
                    }

                }
            },
        ],

        'db' => $db_stage,

        'opencloud' => [
            'class' => 'app\api\modules\v1\components\Opencloud',
            'params' => [
                'container' => '',
                'container_http' => '',
                'region' => 'ORD',
                'api_username' => '',
                'api_key' => '',
            ]
        ],
        'segmentio' => [
            'class' => 'app\api\modules\v1\components\Segmentio',
            'params' => [
                'write_key' => '',
                'read_key' => '',
                'intercom' => '',
            ],
        ],
        'segmentApi' => [
            'class' => 'app\api\modules\v1\components\SegmentApi',
            'params' => [
                'write_key' => '',
                'read_key' => '',
                'intercom' => '',
            ],
        ],
        'pixabay' => [
            'class' => 'app\api\modules\v1\components\Pixabay',
            'params' => [
                'api_key' => '',
            ]
        ],
        'dynamicDbConnection' => [
            'class' => 'app\components\DynamicDbConnection'
        ],
        'mailer' => [
            'class' => '\app\api\modules\v1\components\mailer\Mailer',
            'domain' => '',
            'apiKey' => '',
        ],
    ],

    'modules' => [
        'debug' => [
            'class' => 'yii\debug\Module',
            // uncomment and adjust the following to add your IP if you are not connecting from localhost.
            'allowedIPs' => ['*'],
        ],
        'v1' => ['class' => 'app\api\modules\v1\Module'],
        'gac' => ['class' => 'app\modules\gac\Module'],
        'gii' => ['class' => 'yii\gii\Module']
    ],

    'params' => $params,
];


return $config;