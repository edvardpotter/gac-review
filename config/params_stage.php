<?php

Yii::setAlias('@runtime', realpath(dirname(__FILE__) . '/../runtime'));
Yii::setAlias('@logs', realpath(dirname(__FILE__) . '/../runtime/logs'));
Yii::setAlias('@app', realpath(dirname(__FILE__) . '/..'));
Yii::setAlias('@config', realpath(dirname(__FILE__)));

return [
    'now' => '', //will be initialized in GlobalController
    'dbname' => '', //will be initialized in GlobalController
    'dbname_ar' => '', //will be initialized in MyActiveRecord
    'platform' => 'stage',
    'is_stage' => true,
    'is_console' => ((php_sapi_name() == 'cli') ? true : false),
    'domainUrl' => 'https://app-dev.getacopywriter.com',
    'domainUrlOld' => 'https://vi.getacopywriter.com',
    'apiUrl' => 'https://api-dev.getacopywriter.com',
    'adminEmail' => ['a@getacopywriter.com' => 'Andy'],
    'supportEmail' => ['support@getacopywriter.com' => 'Support Team'],
    'errorEmail' => ['a@getacopywriter.com' => 'Andy'],
    'fromEmail' => ['team@e.getacopywriter.com' => 'Team Get A Copywriter [stage]'], //'team@e.getacopywriter.com',
    'throttlingLimit' => 100,
    'emailSubjectPrefix' => '(DEV) ',
    'uploadFileFolder' => '/var/www-file-upload/', //also configured in php.ini


    'exception' => [
        400 => 'Your request is invalid %s.',
        401 => 'Authorization failed %s.',
        403 => 'Operation is not permitted %s',
        404 => 'Not found %s',
    ],

    'authorization' => [
        'validateHttpHeader' => true,
        'authenticateWithoutHeader' => [        //These endpoints (controller/action) allowed to be used without passing HTTP header
            'tools/ping-no-key',
            'copywriters/token',
            'clients/test',
            'test/url',
        ],
        //These endpoints (controller/action) allowed to be used without passing accounts.unique_id
        'authenticateWithoutAccount' => [
            'GET,companies/invoice',
            'tools/feedback',
            'GET,tools/ping', 'POST,tools/ping', 'PUT,tools/ping',
            'GET,test/url',
        ]
    ],
    'segmentio' => true,    //true to use Segmentio integration within a whole site
    'segmentApi' => true,    //true to use Segmentio integration within a whole site
    'segmentioUniqueID' => true,    //true to use unique ID for tracking
    'patterns' => [
        'first_name' => '/^[0-9a-zA-Z \-\!\#\$\%\&\'\*\+\-\/\=\?\^\_\`\{\|\}\~\@\.\[\]]+$/iu',
        'last_name' => '/^[0-9a-zA-Z \-\!\#\$\%\&\'\*\+\-\/\=\?\^\_\`\{\|\}\~\@\.\[\]]+$/iu',
        'email' => '/^[0-9a-z\-\!\#\$\%\&\'\*\+\-\/\=\?\^\_\`\{\|\}\~\@\.\[\]]+@[0-9a-z_\-^.]+.[a-z]{2,3}$/iu',
        'googleDocs' => '/[A-Za-z]+:\/\/(docs.google.com|drive.google.com)+[A-Za-z0-9-_:%&;\?#\/=]+/iu',
        'googleBody' => '/<body(.*?)>/si',                               //Used for google docs sharing check
        'scriptTag' => '/<script(.*?)>(.*?)<\/script>/si',              //used for google docs sharing check (stripping all script tags)
        'titleTag' => '/<title[^>]*>(.*?)<\/title>/si',                //used for google docs sharing check (retrieve title)
        'leadGarbage' => '/(n\/a[.]*|unknown|noemail@gmail.com)/i',       //Remove unwanted words during leads retrieval
        'matchEmail' => '/[\w.-]+@[\w.-]+\.[a-zA-Z]{2,6}/iu',            //Used to extract/match email address within selected text
        'matchProjectID' => '/(?:^|\D)(\d{5,6})(?=\D|$)/iu',                 //Used to extract/match project ID (exact 5 or 6 digits)
        'matchPhone' => '/((?:\+|00)[17](?: |\-)?|(?:\+|00)[1-9]\d{0,2}(?: |\-)?|(?:\+|00)1\-\d{3}(?: |\-)?)?(0\d|\([0-9]{3}\)|[1-9]{0,3})(?:((?: |\-)[0-9]{2}){4}|((?:[0-9]{2}){4})|((?: |\-)[0-9]{3}(?: |\-)[0-9]{4})|([0-9]{7}))/iu', //Used to extract/match phone number
        'filename' => '/[^\w\s\d\-_\(\).]/',                           //used to replace none valid characters from filename //'/[^\w\s\d\-_~,;:\[\]\(\).]/',
        'comment' => '/<span[^>]*comment=\"(.*?)\"/iu',                //used to extract comments from text
        'plainText' => '/[\r\n]+/i',
        'timezone' => '/[+-][0-9]{2}:[0-9]{2}/',
        'password' => '/^\S{8,}$/iu',
        'fileIds' => '/^(\d+,*)+$/',
    ],
    'executionTime' => [
        'startedAt' => microtime(true),
        'endedOn' => microtime(true),
        'total' => 0
    ],
    'tokenEncryptionKey' => '"7}L~g}TfG^WZ{k*',
    'tokenExpirePeriod' => 168, //hours


//Accessing to logged user and type across platform via Yii::$app->params['user']['id']. Values assigned at ApiUsers
    'user' => [
        'anonymousId' => '',
        'id' => 0,
        'type' => '',
        'accountId' => 0,
        'role' => '',
        'timezone' => '',
        'ip' => '',
    ]

];
